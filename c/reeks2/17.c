#include <stdio.h>

#define N 2

/*
Gebruik matrices met maximaal N rijen en N kolommen - gebruik een constante voor N. Je mag
veronderstellen dat het aantal rijen en kolommen steeds kleiner is dan deze constante (moet je
dus niet controleren).

Schrijf onderstaande proceduren en test ze uit in een hoofdprogramma.

- De procedure lees_matrix(matrix, aant_rij, aant_kol) leest een matrix van gehele
getallen in.

- De procedure print_matrix(matrix, aant_rij, aant_kol) schrijft een matrix van gehele
getallen mooi uit (rij per rij naar het scherm schrijven: 1 lijn is 1 rij).

- De procedure transponeer(matrix, dimensie) transponeert een vierkante matrix. Bij
het transponeren worden de rijen en de kolommen gewisseld. De rijen worden dus de
kolommen en vice versa.

- De procedure vermenigvuldig_matrix(product, matrix1, matrix2, dimensie)
vermenigvuldigt twee vierkante matrices van gehele getallen met elkaar. Je mag veronderstellen
dat beide matrices even groot zijn.
*/

void read_matrix(int[][N], int, int);
void print_matrix(int[][N], int, int);
void transpose(int[][N], int);
void matrix_mult(int[][N], int[][N], int[][N], int);

int main(int argc, char *argv[])
{
  int a[N][N], b[N][N], c[N][N];

  printf("matrix A:\n");
  read_matrix(a, N, N);

  printf("\nmatrix B:\n");
  read_matrix(b, N, N);

  printf("\nA transposed:\n");
  transpose(a, N);
  print_matrix(a, N, N);

  printf("\nB transposed:\n");
  transpose(b, N);
  print_matrix(b, N, N);

  printf("\nA x B:\n");
  matrix_mult(c, a, b, N);
  print_matrix(c, N, N);
}

void read_matrix(int m[][N], int r, int c)
{
  int i, j;
  for (i = 0; i < r; i++)
    for (j = 0; j < c; j++)
      scanf("%d", &m[i][j]);
  fflush(stdin);
}

void print_matrix(int m[][N], int r, int c)
{
  int i, j;
  for (i = 0; i < r; i++)
  {
    for (j = 0; j < c; j++)
      printf("%d ", m[i][j]);
    printf("\n");
  }
}

void transpose(int m[][N], int d)
{
  int i, j, tmp;
  for (i = 0; i < d; i++)
    for (j = i; j < d; j++)
      if (i != j)
      {
        tmp = m[i][j];
        m[i][j] = m[j][i];
        m[j][i] = tmp;
      }
}

void matrix_mult(int y[][N], int m[][N], int n[][N], int d)
{
  int i, j, k;
  for (i = 0; i < d; i++)
    for (j = 0; j < d; j++)
    {
      y[i][j] = 0;
      for (k = 0; k < d; k++)
        y[i][j] += m[i][k] * n[k][j];
    }
}
