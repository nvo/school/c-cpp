#include <stdio.h>

/*
Schrijf voor het eerste deel van deze oefening ENKEL een hoofdprogramma, geen functies of
procedures. (Houd je hier aan, of je mist de clou van de oefening.)

  1.  Maak in het hoofdprogramma een array aan waarin elk element een karakter is. Vul deze
      array bij declaratie op als volgt:

      char letters [] = {'p','o','r','e','o','i','f','o','i','e','c','i',
      'i',':','a','-','t','('};

      Bepaal de lengte van de array zonder te tellen (zie theorie)! Schrijf in het hoofdprogramma
      alle karakters uit die op een even positie in de array staan.

  2.  Schrijf nu een procedure schrijf_even_posities(...) die alle karakters uitschrijft die op
      een even positie in de gegeven array staan (Bepaal zelf aantal en aard van de parameters).
      Roep deze procedure op in het hoofdprogramma en test grondig uit.
*/

void print_evens(char[], int);

int main(int argc, char *argv[])
{
  char letters[] = {'p', 'o', 'r', 'e', 'o', 'i', 'f', 'o', 'i', 'e', 'c', 'i', 'i', ':', 'a', '-', 't', '('};
  int n = sizeof(letters) / sizeof(char);

  int i;
  for (i = 0; i < n; i += 2)
    printf("%c", letters[i]);
  printf("\n");

  print_evens(letters, n);
}

void print_evens(char str[], int n)
{
  int i;
  for (i = 0; i < n; i += 2)
    printf("%c", str[i]);
  printf("\n");
}
