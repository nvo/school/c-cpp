#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 20
#define MIN 100
#define MAX 120
#define RN (MAX - MIN + 1)

/*
Schrijf een programma dat 20 gehele getallen genereert tussen 100 en 120 (grenzen inbegrepen) en
deze ook op het scherm toont. Daarna worden alle getallen die niet gekozen werden, in stijgende
volgorde uitgeschreven. Kijk kritisch na!

Heb je de grenzen en het aantal te genereren getallen in constanten bewaard?
*/

int main(int argc, char *argv[])
{
  time_t t;
  srand((unsigned int)time(&t));

  int i, numbers[RN];
  for (i = 0; i < N; i++)
    numbers[rand() % (RN) + MIN]++;

  for (i = 0; i < RN; i++)
    if (!numbers[i])
      printf("%d ", MIN + i);
}
