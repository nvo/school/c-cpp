#include <stdio.h>

/*
Schrijf een procedure schuif_links(t,n) die alle elementen van een gegeven array t met n
lettertekens, één plaats naar links schuift. Het eerste karakter komt achteraan. Roep deze
procedure drie keer aan op de array

  char rij[] = {'s','a','p','a','p','p','e','l'};

Aangezien je de array verschillende keren moet uitschrijven, voorzie je hiervoor uiteraard een
procedure.
*/

void shift_left(char[], int);
void print_arr(char[], int);

int main(int argc, char *argv[])
{
  char rij[] = {'s', 'a', 'p', 'a', 'p', 'p', 'e', 'l'};
  int n = sizeof(rij) / sizeof(char);

  int i;
  for (i = 0; i < 3; i++)
  {
    shift_left(rij, n);
    print_arr(rij, n);
  }
}

void shift_left(char t[], int n)
{
  if (n < 1)
    return;

  char first = t[0];

  int i;
  for (i = 1; i < n; i++)
    t[i - 1] = t[i];
  t[n - 1] = first;
}

void print_arr(char t[], int n)
{
  int i;
  for (i = 0; i < n; i++)
    printf("%c ", t[i]);
  printf("\n");
}
