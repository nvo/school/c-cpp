#include <stdio.h>
#include <math.h>

#define THRESHOLD 1e-9

/*
1.  Schrijf een functie index_van(t, n, g), die de (kleinste) index teruggeeft van de plaats
    waarop een gegeven reëel getal g in een gegeven array t van n reele getallen gevonden wordt.
    Indien het getal niet aanwezig is, wordt er -1 teruggegeven.
    Test uit! Declareer een array (hardgecodeerd). Lees een getal in en zoek dit getal op in de
    array.

2.  Doe de nodige aanpassingen zodat je programma controleert of de gebruiker een reëel getal
    ingeeft (Indien iets wordt ingegeven dat niet aan de voorwaarden voldoet, dan blijft het
    programma vragen naar een reeel getal tot het ingegeven wordt).
*/

int index_of(double t[], int n, double g);

int main(int argc, char *argv[])
{
  double arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  double x;

  do
  {
    fflush(stdin);
    printf("Enter number: ");
  } while (scanf("%lf", &x) <= 0);

  printf("index of %.2f: %d\n", x, index_of(arr, sizeof(arr) / sizeof(double), x));
}

int index_of(double t[], int n, double g)
{
  int i;
  for (i = 0; i < n; i++)
    if (fabs(g - t[i]) < THRESHOLD)
      return i;
  return -1;
}
