#include <stdio.h>
#include <ctype.h>

#define N 26

/*
Schrijf een programma dat tekstuele input van de gebruiker, afgesloten met een $-teken, verwerkt
(zie voorwaarden na het voorbeeld!). Van elke letter wordt bijgehouden hoe dikwijls hij
voorkomt. Hierbij worden hoofdletters meegerekend bij de overeenkomstige kleine letters.

Daarna worden twee histogrammen getekend (zie voorbeeld in pdf).

Voorwaarden waaraan je oplossing moet voldoen:
- Je schrijft en gebruikt drie procedures: één die de tekst inleest/verwerkt, één die een horizontaal
staafdiagram tekent en één die een vertikaal staafdiagram tekent.

- Je gebruikt, buiten scanf en printf, geen bibliotheekfuncties. Zelf extra hulpfuncties
schrijven kan wel zinvol zijn.

Laat nu je programma vanaf de command line draaien, waarbij je input-redirection gebruikt om
de input te halen uit het bestand gandhi.txt.

Informatie over input-redirection vind je opUfora - Labo - Algemeen
*/

void process_input(int[], int);
void horizontal(int[], int);
void vertical(int[], int);

int main(int argc, char *argv[])
{
  int t[N] = {0};

  // read input
  process_input(t, N);
  printf("\n");

  // print horizontal histogram
  horizontal(t, N);
  printf("\n");

  // print vertical histogram
  vertical(t, N);
}

void process_input(int t[], int n)
{
  char c;

  // count letters
  while ((c = tolower(getchar())) != '$')
    t[c - 'a']++;
}

void horizontal(int t[], int n)
{
  int i, j;
  for (i = 0; i < n; i++)
  {
    printf("%c: ", 'a' + i);
    for (j = 0; j < t[i]; j++)
      printf("*");
    printf("\n");
  }
}

void vertical(int t[], int n)
{
  if (n < 1)
    return;

  int i, j, max = t[0];
  for (i = 1; i < n; i++)
    if (t[i] > max)
      max = t[i];

  for (i = max; i > 0; i--)
  {
    for (j = 0; j < n; j++)
      printf("%c", t[j] >= i ? ('a' + j) : ' ');
    printf("\n");
  }
}
