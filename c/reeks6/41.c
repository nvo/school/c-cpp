/*
In bovenstaand hoofdprogramma wordt er automatisch een stijgend gesorteerde lijst aangemaakt,
waar dubbele elementen in kunnen zitten. Implementeer de functie
maak_gesorteerde_lijst_automatisch(aantal,bovengrens). De eerste parameter geeft aan
hoeveel elementen de lijst moet bevatten. De tweede parameter geeft aan wat het grootste getal
zal zijn. Een tip: bouw de lijst op door vooraan telkens een iets kleiner getal dan het vorige
toe te voegen. Daarvoor genereer je een random getal uit de verzameling f0; 1; 2g en trek je dit
getal af van het vorige toegevoegde getal (of van het getal bovengrens bij het toevoegen van de
eerste knoop).

Schrijf de procedure verwijder_dubbels(...) die alle dubbels uit de gelinkte lijst verwijdert.
Als enige parameter geef je de gelinkte lijst mee. Vul het hoofdprogramma verder aan zoals het
hoort.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIFF 3

typedef struct knoop knoop;
struct knoop
{
  int d;
  knoop *opv;
};

void voeg_vooraan_toe(int x, knoop **k);
void print_lijst(knoop *k);
void free_lijst(knoop *k);

knoop *maak_gesorteerde_lijst_automatisch(int, int);
void verwijder_dubbels(knoop *k);

int main()
{
  srand(time(NULL));

  knoop *l = maak_gesorteerde_lijst_automatisch(10, 100);
  print_lijst(l);

  printf("\nnu worden dubbels verwijderd: \n");
  verwijder_dubbels(l); /* aan te vullen */

  printf("\nna verwijderen van dubbels: \n\n");
  print_lijst(l);

  free_lijst(l);

  return 0;
}

knoop *maak_gesorteerde_lijst_automatisch(int n, int bound)
{
  knoop *l = 0;

  int i;
  for (i = 0; i < n; i++)
    voeg_vooraan_toe((bound -= rand() % DIFF), &l);

  return l;
}

void verwijder_dubbels(knoop *k)
{
  knoop *l = k;
  knoop *prev = k;
  k = k->opv;
  while (k)
  {
    if (prev->d == k->d)
    {
      prev->opv = k->opv;
      free(k);
      k = prev->opv;
    }
    else
    {
      prev = k;
      k = k->opv;
    }
  }
}

void voeg_vooraan_toe(int x, knoop **k)
{
  knoop *n = malloc(sizeof(knoop *));
  n->d = x;
  n->opv = *k;

  *k = n;
}

void print_lijst(knoop *k)
{
  while (k)
  {
    printf("%d ", k->d);
    k = k->opv;
  }
  printf("\n");
}

void free_lijst(knoop *k)
{
  knoop *opv;
  while (k)
  {
    opv = k->opv;
    free(k);

    k = opv;
  }
}
