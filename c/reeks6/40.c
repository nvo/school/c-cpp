#include <stdio.h>
#include <stdlib.h>

/*
Schrijf de nodige procedures, opdat bovenstaand hoofdprogramma zou werken. Op de puntjes
geef je de gelinkte lijst l mee. Denk na over de manier waarop je dat doet.

  1.  De procedure voeg_vooraan_toe breidt de gegeven gelinkte lijst (tweede parameter) uit
      met één knoop: die komt vooraan, en bevat het gegeven getal (eerste parameter).
  2.  Implementeer de procedure print_lijst.
  3.  Er ontbreekt een procedure aan het hoofdprogramma. Schrijf hoofding, implementatie en
      aanroep van deze procedure.
*/

typedef struct knoop knoop;
struct knoop
{
  int d;
  knoop *opv;
};

void voeg_vooraan_toe(int x, knoop **k);
void print_lijst(knoop *k);
void free_lijst(knoop *k);

int main()
{
  knoop *l = 0;
  voeg_vooraan_toe(7, &l);
  voeg_vooraan_toe(3, &l);
  print_lijst(l);

  free_lijst(l);
  return 0;
}

void voeg_vooraan_toe(int x, knoop **k)
{
  knoop *n = malloc(sizeof(knoop *));
  n->d = x;
  n->opv = *k;

  *k = n;
}

void print_lijst(knoop *k)
{
  while (k)
  {
    printf("%d ", k->d);
    k = k->opv;
  }
}

void free_lijst(knoop *k)
{
  knoop *opv;
  while (k)
  {
    opv = k->opv;
    free(k);

    k = opv;
  }
}
