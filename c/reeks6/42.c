/*
Werk verder op voorgaande oefening. Het hoofprogramma wordt nu:
...

Schrijf de functie merge die twee gerangschikte lijsten als parameter neemt. Beide lijsten zullen
op het einde leeg zijn; hun knopen zijn allemaal verplaatst (niet gekopiëerd) naar de nieuwe (eveneens
gerangschikte) lijst die teruggegeven wordt. Omdat de lijsten die je meegeeft gerangschikt
moeten zijn (precondities van de functie), kan je de lijsten efficiënt samenvoegen. Neem hiervoor
onderstaande code, die hetzelfde doet voor twee array's a en b als voorbeeld.

int * merge(const int * a, const int * b, int size_a, int size_b){
  int i = 0;  // indexeert a
  int j = 0;  // indexeert b
  int k = 0;  // indexeert c

  int size_c = size_a + size_b;
  int *c = (int *)malloc(size_c * sizeof(int));

  while (i < size_a && j < size_b)
    if (a[i] < b[j])
      c[k++] = a[i++];
    else
      c[k++] = b[j++];

  while (i < size_a)
    c[k++] = a[i++];

  while (j < size_b)
    c[k++] = b[j++];

  return c;
}
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIFF 3

typedef struct knoop knoop;
struct knoop
{
  int d;
  knoop *opv;
};

void voeg_vooraan_toe(int x, knoop **k);
void print_lijst(knoop *k);
void free_lijst(knoop *k);

knoop *maak_gesorteerde_lijst_automatisch(int, int);
void verwijder_dubbels(knoop *k);

knoop *merge(knoop **a, knoop **b);

int main()
{
  srand(time(NULL));

  knoop *m = maak_gesorteerde_lijst_automatisch(10, 1000);
  knoop *n = maak_gesorteerde_lijst_automatisch(5, 1000);

  printf("\nLIJST m:\n");
  print_lijst(m);

  printf("\nLIJST n:\n");
  print_lijst(n);

  printf("\nDeze worden gemerged. \n\n");
  knoop *mn = merge(&m, &n);

  printf("\nLIJST m: \n");
  print_lijst(m);

  printf("\nLIJST n: \n");
  print_lijst(n);

  printf("\nRESULTAAT: \n");
  print_lijst(mn);

  free_lijst(m);
  free_lijst(n);
  free_lijst(mn);
  return 0;
}

knoop *merge(knoop **a, knoop **b)
{
  knoop *c;
  knoop *l;

  if ((*a)->d < (*b)->d)
  {
    c = *a;
    *a = (*a)->opv;
  }
  else
  {
    c = *b;
    *b = (*b)->opv;
  }

  l = c;

  while (*a && *b)
  {
    if ((*a)->d < (*b)->d)
    {
      c->opv = *a;
      *a = (*a)->opv;
    }
    else
    {
      c->opv = *b;
      *b = (*b)->opv;
    }
    c = c->opv;
  }

  while (*a)
  {
    c->opv = *a;
    *a = (*a)->opv;
    c = c->opv;
  }

  while (*b)
  {
    c->opv = *b;
    *b = (*b)->opv;
    c = c->opv;
  }

  a = 0;
  b = 0;

  return l;
}

knoop *maak_gesorteerde_lijst_automatisch(int n, int bound)
{
  knoop *l = 0;

  int i;
  for (i = 0; i < n; i++)
    voeg_vooraan_toe((bound -= rand() % DIFF), &l);

  return l;
}

void verwijder_dubbels(knoop *k)
{
  knoop *l = k;
  knoop *prev = k;
  k = k->opv;
  while (k)
  {
    if (prev->d == k->d)
    {
      prev->opv = k->opv;
      free(k);
      k = prev->opv;
    }
    else
    {
      prev = k;
      k = k->opv;
    }
  }
}

void voeg_vooraan_toe(int x, knoop **k)
{
  knoop *n = malloc(sizeof(knoop));
  n->d = x;
  n->opv = *k;

  *k = n;
}

void print_lijst(knoop *k)
{
  while (k)
  {
    printf("%d ", k->d);
    k = k->opv;
  }
  printf("\n");
}

void free_lijst(knoop *k)
{
  knoop *opv;
  while (k)
  {
    opv = k->opv;
    free(k);

    k = opv;
  }
}
