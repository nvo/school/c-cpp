/*
Hieronder de procedure voeg_getal_toe die een knoop toevoegt aan een lijst die stijgend geordend
is en moet blijven.

void voeg_getal_toe(int g, knoop **pl)
{
  knoop *h, *m;
  if (*pl == 0 || g <= (*pl)->d)
  {
    h = (knoop *)malloc(sizeof(knoop));
    h->d = g;
    h->opv = *pl;
    *pl = h;
  }
  else
  {
    h = *pl;

    while (h->opv != 0 && h->opv->d < g)
      h = h->opv;

    m = h->opv;
    h->opv = (knoop *)malloc(sizeof(knoop));

    h = h->opv;
    h->d = g;
    h->opv = m;
  }
}

Je ziet dat er een opsplitsing nodig is in deelgevallen: een stuk code dat van toepassing is indien
het toevoegen vooraan in de lijst gebeurt, en een stuk code dat alle andere gevallen behandelt.
Dit dubbele werk willen we vermijden. Het is immers werk gespaard als je code kan schrijven
die in elke situatie bruikbaar is.
De hulppointer h werd gedeclareerd als knoop * en duidt de knoop aan waar de nieuwe knoop
aangehangen zal worden. Op tekening:
zie pdf

Als we het getal 4 willen toevoegen, zal h naar de eerste knoop wijzen. Als we het getal 10
toevoegen, zal h naar de laatste knoop wijzen. Als we het getal 2 willen toevoegen, zal h geen
knoop hebben om naar te wijzen. (Vandaar de opsplitsing in gevallen in de code.)

Het valt echter op te merken dat je enkel h->opv nodig hebt in de code. De inhoud van de knoop
waar h naar wijst (h->d) heb je niet nodig. Waarom dan naar de hele knoop (zowel h->d als
h->opv) wijzen? Is toegang tot h->opv niet voldoende? Dat is de oplossing voor ons probleem:
we laten een hulppointer k wijzen naar de horizontale pijlen (de links tussen de knopen). Willen
we 4 toevoegen, dan wijst k naar de tweede horizontale pijl. Willen we 10 toevoegen, dan wijst
k naar de laatste horizontale pijl. Willen we 2 toevoegen, dan wijst k naar de eerste horizontale
pijl. En het behandelen van een speciaal geval is niet meer nodig!

Herschrijf de procedure voeg_getal_toe(getal,lijst) die een knoop met inhoud getal toevoegt
aan een lijst die ondersteld wordt stijgend geordend te zijn (en te blijven). Dubbels zijn
toegelaten. Gebruik geen if-else -structuur.
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct knoop knoop;
struct knoop
{
  int d;
  knoop *opv;
};

void voeg_getal_toe(int g, knoop **pl);
void print_lijst(knoop *k);
void free_lijst(knoop *k);

int main()
{
  knoop *l = malloc(sizeof(knoop));
  l->d = 10;
  l->opv = 0;

  int i;
  for (i = 9; i > 0; i--)
    voeg_getal_toe(i, &l);

  print_lijst(l);
  free_lijst(l);
}

void voeg_getal_toe(int g, knoop **pl)
{
  knoop *k;

  while ((*pl) && (*pl)->d < g)
    *pl = (*pl)->opv;

  k = *pl;

  *pl = malloc(sizeof(knoop));
  (*pl)->d = g;
  (*pl)->opv = k;
}

void print_lijst(knoop *k)
{
  while (k)
  {
    printf("%d ", k->d);
    k = k->opv;
  }
  printf("\n");
}

void free_lijst(knoop *k)
{
  knoop *opv;
  while (k)
  {
    opv = k->opv;
    free(k);

    k = opv;
  }
}