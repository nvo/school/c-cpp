typedef struct knoop knoop;
struct knoop
{
  int d;
  knoop *opv;
};

int main()
{
  knoop *l = 0;
  knoop *k = l;
  k = (knoop *)malloc(sizeof(knoop));
  return 0;
}

/*
Teken de twee pointers l en k, en wat er precies gebeurt in bovenstaande code. Is l gewijzigd?

1. l -> NULL
2. k -> NULL
3. k -> {ADRES naar KNOOP}

Enkel k wordt gewijzigd.
*/