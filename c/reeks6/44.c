/*
Werk je sinds oefening 39 in een nieuw bestand? Heb je dan gedacht aan opkuisen van de lijst?
Schrijf nu, als je dat nog niet deed, een recursieve versie van de procedure die een lijst volledig
vrijgeeft en test uit. (Schrijf net voor het vrijgeven van een knoop, diens inhoud uit.)
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct knoop knoop;
struct knoop
{
  int d;
  knoop *opv;
};

void voeg_vooraan_toe(int x, knoop **k);
void print_lijst(knoop *k);
void free_lijst(knoop *k);

int main()
{
  knoop *l = 0;
  voeg_vooraan_toe(7, &l);
  voeg_vooraan_toe(3, &l);
  print_lijst(l);

  free_lijst(l);
  return 0;
}

void voeg_vooraan_toe(int x, knoop **k)
{
  knoop *n = malloc(sizeof(knoop *));
  n->d = x;
  n->opv = *k;

  *k = n;
}

void print_lijst(knoop *k)
{
  while (k)
  {
    printf("%d ", k->d);
    k = k->opv;
  }
  printf("\n");
}

void free_lijst(knoop *k)
{
  if (!k)
    return;

  free_lijst(k->opv);
  printf("{d: %d, opv: %p}\n", k->d, k->opv);
  free(k);
}
