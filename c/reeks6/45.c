/*
Schrijf een procedure verwijder(x,lijst) die - indien aanwezig - één knoop met inhoud x
uit de stijgend gerangschikte lijst `lijst` verwijdert.

Schrijf ook een recursieve versie van deze procedure.
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct knoop knoop;
struct knoop
{
  int d;
  knoop *opv;
};

void verwijder(int x, knoop **k);
void voeg_vooraan_toe(int x, knoop **k);
void print_lijst(knoop *k);
void free_lijst(knoop *k);

int main()
{
  knoop *l = 0;
  voeg_vooraan_toe(8, &l);
  voeg_vooraan_toe(7, &l);
  voeg_vooraan_toe(6, &l);
  voeg_vooraan_toe(4, &l);
  voeg_vooraan_toe(3, &l);
  voeg_vooraan_toe(3, &l);
  voeg_vooraan_toe(2, &l);
  print_lijst(l);

  verwijder(1, &l);
  print_lijst(l);

  verwijder(2, &l);
  print_lijst(l);

  verwijder(3, &l);
  print_lijst(l);

  verwijder(5, &l);
  print_lijst(l);

  free_lijst(l);
  return 0;
}

void verwijder(int x, knoop **k)
{
  knoop *p;
  while (*k && (*k)->d < x)
    k = &((*k)->opv);

  if ((*k)->d > x)
    return;

  p = *k;
  *k = (*k)->opv;
  free(p);
}

void voeg_vooraan_toe(int x, knoop **k)
{
  knoop *n = malloc(sizeof(knoop *));
  n->d = x;
  n->opv = *k;

  *k = n;
}

void print_lijst(knoop *k)
{
  while (k)
  {
    printf("%d ", k->d);
    k = k->opv;
  }

  printf("\n");
}

void free_lijst(knoop *k)
{
  knoop *opv;
  while (k)
  {
    opv = k->opv;
    free(k);

    k = opv;
  }
}
