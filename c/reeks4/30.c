#include <stdio.h>
#include <string.h>

/*
Gegeven een gedeelte van een hoofdprogramma:

1.  Schrijf één logische functie array_bevat_dubbels_na_elkaar. Deze functie bepaalt voor
    een array met elementen van een niet nader bepaald type, of er ergens in die array twee elementen
    vlak na elkaar staan die gelijk zijn. In ons voorbeeld zal dat wel zo zijn voor de arrays
    getallen_ja en woorden_ja, maar niet voor de arrays getallen_neen en woorden_neen.

2.  Schrijf ook de nodige hulpfuncties om ervoor te zorgen dat je het hoofdprogramma volledig
    werkend krijgt.

3.  Tenslotte vul je het hoofdprogramma aan: roep de functie array_bevat_dubbels_na_elkaar
    vier keer op (voor elke gedeclareerde array 1 keer) en ga na of het programma de juiste output
    oplevert.
*/

int has_consecutive_doubles(const void *, int, int, int (*)(const void *, const void *));
int equals_int(const void *, const void *);
int equals_cstring(const void *, const void *);

int main()
{
  int getallen_ja[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 11};
  int getallen_neen[] = {1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 3, 4, 5};

  char *woorden_ja[] = {"leentje", "leerde", "lotje", "lopen", "lopen", "langs", "de", "lindelaan"};
  char *woorden_neen[] = {"wie", "goed", "doet", "goed", "ontmoet"};

  // voor elk van deze vier arrays wordt er opgeroepen:
  if (has_consecutive_doubles((void *)getallen_ja, sizeof(getallen_ja) / sizeof(int), sizeof(int), equals_int))
    printf("getallen_ja bevat dubbels na elkaar\n");
  else
    printf("getallen_ja bevat geen dubbels na elkaar\n");

  if (has_consecutive_doubles((void *)getallen_neen, sizeof(getallen_neen) / sizeof(int), sizeof(int), equals_int))
    printf("getallen_neen bevat dubbels na elkaar\n");
  else
    printf("getallen_neen bevat geen dubbels na elkaar\n");

  if (has_consecutive_doubles((void *)woorden_ja, sizeof(woorden_ja) / sizeof(char *), sizeof(char *), equals_cstring))
    printf("woorden_ja bevat dubbels na elkaar\n");
  else
    printf("woorden_ja bevat geen dubbels na elkaar\n");

  if (has_consecutive_doubles((void *)woorden_neen, sizeof(woorden_neen) / sizeof(char *), sizeof(char *), equals_cstring))
    printf("woorden_neen bevat dubbels na elkaar\n");
  else
    printf("woorden_neen bevat geen dubbels na elkaar\n");

  return 0;
}

int has_consecutive_doubles(const void *t, int n, int s, int (*equals)(const void *, const void *))
{
  int i;
  for (i = 1; i < n; i++, t += s)
    if (equals(t, t + s))
      return 1;
  return 0;
}

int equals_int(const void *a, const void *b)
{
  return *(int *)a == *(int *)b;
}

int equals_cstring(const void *a, const void *b)
{
  return strcmp(*(char **)a, *(char **)b) == 0;
}
