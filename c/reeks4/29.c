#include <stdio.h>

/*
Intialiseer in het hoofdprogramma volgende arrays:

char * namen[] = {"Evi","Jaro","Timen","Youri","Ashaf","Jennifer"};
int leeftijden[] = {21,30,18,14,22,19};
double scores[] = {0.5,1.6,8.2,-2.4};

We willen de elementen van deze arrays uitschrijven, gescheiden door een leesteken naar keuze.
Omdat de arrays elk van een ander type zijn, zouden we drie keer bijna dezelfde code moeten
schrijven: een lus om de elementen van een array te overlopen en die uit te schrijven.
Dat dubbel werk kunnen we vermijden. Ga daarvoor als volgt te werk:

1.  Schrijf de procedure
      void schrijf_array(const void * t, int aantal, int grootte, char tussenteken, void (*schrijf)(const void*))

    die de eerste aantal elementen uit de array t uitschrijft. Elk koppel elementen wordt
    gescheiden door het opgegeven tussenteken. De parameter grootte bevat de grootte van
    het type dat uitgeschreven moet worden. De parameter schrijf is een pointer naar een
    passende uitschrijfprocedure.

2.  Schrijf drie procedures schrijf_cstring, schrijf_int en schrijf_double die alledrie
    één parameter van een pointertype meekrijgen en één element uitschrijven.

Schrijf een hoofdprogramma dat volgende output produceert:

21,30,18,14,22,19

Evi;Jaro;Timen;Youri;Ashaf;Jennifer

0.5~1.6~8.2~-2.4
*/

void print_array(const void *, int, int, char, void (*)(const void *));
void print_cstring(const void *);
void print_int(const void *);
void print_double(const void *);

int main()
{
  char *namen[] = {"Evi", "Jaro", "Timen", "Youri", "Ashaf", "Jennifer"};
  int leeftijden[] = {21, 30, 18, 14, 22, 19};
  double scores[] = {0.5, 1.6, 8.2, -2.4};

  int n_namen = sizeof(namen) / sizeof(char *);
  int n_leeftijden = sizeof(leeftijden) / sizeof(int);
  int n_scores = sizeof(namen) / sizeof(double);

  print_array((void *)leeftijden, n_leeftijden, sizeof(int), ',', print_int);
  print_array((void *)namen, n_namen, sizeof(char *), ';', print_cstring);
  print_array((void *)scores, n_scores, sizeof(double), '~', print_double);
}

void print_array(const void *t, int n, int s, char d, void (*print)(const void *))
{
  if (s < 1)
    return;

  int i;
  print(t);
  for (i = 1; i < n; i++)
  {
    printf("%c", d);
    print((t += s));
  }
  printf("\n");
}

void print_cstring(const void *p)
{
  char **str = (char **)p;
  printf("%s", *str);
}

void print_int(const void *p)
{
  int *d = (int *)p;
  printf("%d", *d);
}

void print_double(const void *p)
{
  double *lf = (double *)p;
  printf("%.1lf", *lf);
}
