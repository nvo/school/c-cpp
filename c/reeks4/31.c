#include <stdio.h>

/*
Schrijf een programma dat je bewaart onder de naam begroet.c. Op de commandolijn zal je,
na de naam van het programma, een aantal persoonsnamen opgeven. Het programma antwoordt
dan met een groet aan alle vermelde personen. Elke naam moet met een hoofdletter starten en
omgezet worden zoals in oefening 26 (hergebruik code).

Voorbeeld:
Geef op de commandolijn:      begroet oriana thomas han

Dan komt er als reply:            Dag Oriana!
                                  Dag Thomas!
                                  Dag Han!
Worden er geen namen meegegeven,
dan komt er                       Dag allemaal!.
*/

void my_toupper(char *);

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    printf("Dag allemaal!\n");
    return 0;
  }

  int i;
  for (i = 1; i < argc; i++)
  {
    my_toupper(argv[i]);
    printf("Dag %s!\n", argv[i]);
  }
}

void my_toupper(char *s)
{
  // set uppercase letter
  if (*s >= 'a' && *s <= 'z')
    *s += 'A' - 'a';

  while (*++s != '\0')
    if (*s >= 'A' && *s <= 'Z')
      *s += 'a' - 'A';
}
