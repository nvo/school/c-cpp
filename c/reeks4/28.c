#include <stdio.h>
#include <ctype.h>

#define N 128

/*
Schrijf een procedure wis(s) die uit de string s alle tekens wist die geen kleine letter of geen
white space zijn. Je mag gebruik maken van de functies islower(char) en isspace(char).

Test uit met de string
"8d'a7!<t-)>+. -)4h&!e9)b*( )j'(e)!4\n8g|'92o!43e5d/.' 2 3g*(e('d22a'(a25n'(".

Test ook uit met een zinnetje dat je inleest (gebruik spaties in je input).v
*/

void erase(char *);

int main()
{
  char s[N] = "8d'a7!<t-)>+. -)4h&!e9)b*( )j'(e)!4\n8g|'92o!43e5d/.' 2 3g*(e('d22a'(a25n'(";
  erase(s);
  printf("%s\n", s);

  printf("Enter text: ");
  fgets(s, N, stdin);
  erase(s);
  printf("%s\n", s);
}

void erase(char *s)
{
  char *t = s;
  while (*s++)
    if (islower(*s) || isspace(*s))
      *t++ = *s;
  *t = 0;
}
