#include <stdio.h>

#define MAX 128

/*
Schrijf een procedure my_toupper(s) die het eerste karakter van de gegeven C-string s omzet
naar een hoofdletter (indien dit eerste karakter een kleine letter is), en de andere letters naar
een kleine letter. Cijfertekens en leestekens worden niet beïnvloed. Je mag er vanuit gaan dat
het eerste karakter een letter is. Gebruik schuivende pointers.

Test uit met een hoofdprogramma waarin je het woord snEEuwwITJE<3!! laat omzetten naar
Sneeuwwitje<3!!. Daarna test je ook uit met een woord dat je inleest van het toetsenbord.
*/

void my_toupper(char *);

int main(int argc, char *argv[])
{
  // char s[] = "snEEuwwITJE<3!!";
  char s[256];

  printf("Enter text: ");
  scanf("%s", s);
  fflush(stdin);

  my_toupper(s);

  printf("%s\n", s);
}

void my_toupper(char *s)
{
  // set uppercase letter
  if (*s >= 'a' && *s <= 'z')
    *s += 'A' - 'a';

  while (*++s != '\0')
    if (*s >= 'A' && *s <= 'Z')
      *s += 'a' - 'A';
}
