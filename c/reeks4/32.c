#include <stdio.h>
#include <string.h>

/*
Werk verder op vorige oefening. Indien het programma op de commandolijn wordt opgeroepen
zonder parameters, dan komt er nog altijd Dag allemaal!. In het andere geval begroet je enkel
de persoon waarvan de naam alfabetisch het eerst komt. Dat zou in het voorbeeld van vorige
opgave dus Han zijn.

Schrijf de functie alfab_kleinste(voornamen,n) die een pointer teruggeeft die wijst naar de
naam die alfabetisch eerst staat in de array van c-strings (met naam voornamen) die als parameter
wordt meegegeven. De tweede parameter is de lengte n van de array.
*/

void my_toupper(char *);
char **alphabet_smallest(char *[], int);

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    printf("Dag allemaal!\n");
    return 0;
  }

  char **name = alphabet_smallest(argv + 1, argc - 1);
  my_toupper(*name);
  printf("Dag %s!\n", *name);
}

void my_toupper(char *s)
{
  // set uppercase letter
  if (*s >= 'a' && *s <= 'z')
    *s += 'A' - 'a';

  while (*++s != '\0')
    if (*s >= 'A' && *s <= 'Z')
      *s += 'a' - 'A';
}

char **alphabet_smallest(char *t[], int n)
{
  if (n < 1)
    return (char **)t;

  char **min = &t[0];
  int i;
  for (i = 1; i < n; i++)
    if (strcmp(t[i], *min) < 0)
      min = &t[i];
  return min;
}
