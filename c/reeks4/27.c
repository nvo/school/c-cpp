#include <stdio.h>
#include <stdlib.h>

/*
Gegeven onderstaand hoofdprogramma (haal eerst het latex-bestand af van Minerva, zodat je
geen code hoeft over te tikken). Schrijf de nodige code om dit te laten werken; gebruik overal
verplicht schuivende pointers. De procedure schrijf(begin,eind) werd al gevraagd in
oefening 21.

1.  de functie pointerNaarEersteKleineLetter(p) geeft een pointer terug naar de eerste
    kleine letter die te vinden is vanaf de huidige positie van de pointer p. Indien er geen kleine
    letters meer volgen, staat p op het einde van de c-string (=voorbij het laatste karakter).

2.  de procedure verzetNaarEersteHoofdletter(p) verzet de gegeven pointer p zodat hij wijst
    naar de eerste hoofdletter die vanaf zijn huidige positie te vinden is. (Ook hier: voorzie de
    situatie waarbij er geen hoofdletters meer volgen.)
*/

void toFirstUpper(char const **);
char const *ptrToFirstLower(char const *);
void print(char const *b, char const *const e);

int main()
{
  char zus1[50] = "sneeuwWITje";
  char zus2[50] = "rozeROOD";

  const char *begin;
  const char *eind;

  begin = zus1;
  toFirstUpper(&begin);
  eind = ptrToFirstLower(begin);
  print(begin, eind); /* schrijft 'WIT' uit */

  printf("\n");

  begin = zus2;
  toFirstUpper(&begin);
  eind = ptrToFirstLower(begin);
  print(begin, eind); /* schrijft 'ROOD' uit */

  return 0;
}

void toFirstUpper(char const **p)
{
  while (**p != '\0' && !(**p >= 'A' && **p <= 'Z'))
    (*p)++;
}

char const *ptrToFirstLower(char const *p)
{
  while (*p != '\0' && !(*p >= 'a' && *p <= 'z'))
    p++;

  return p;
}

void print(char const *b, char const *const e)
{
  do
  {
    printf("%c", *b);
  } while (++b < e);
}
