#include <stdio.h>

/*
Gegeven onderstaand hoofdprogramma. Gebruik de functie schrijf(begin,einde) uit oefening
21)

  main(){
    char tekst[] = {'b','d','?','z','g','o','e','z','e','b',' ','d','i','g','!','h','o','s','v'};
    pivoteer(tekst+7,tekst+12,tekst+9);
    schrijf(tekst+4,tekst+15);
  }

Schrijf de procedure pivoteer(begin,einde,pivot). De drie parameters zijn pointers naar
karakters in dezelfde array. Je mag veronderstellen dat begin < pivot < einde.

De procedure wisselt de elementen symmetrisch rond de pivot. Ligt de pivot niet netjes in het
midden tussen de grenzen begin en einde, dan wordt het wisselen beperkt.
Een voorbeeld: indien de elementen vanaf begin tot net voor einde gelijk zijn aan a b c d e f g h
en pivot wijst naar de letter c, dan wordt dit rijtje na afloop van de procedure e d c b a f g h.

Gebruik schuivende pointers.

Is de uitvoer van je programma geen leesbare uitspraak, dan schort er nog wat aan.
*/

void pivot(char *, char *, char *);
void print(char const *b, char const *const e);

int main(int argc, char *argv[])
{
  char tekst[] = {'b', 'd', '?', 'z', 'g', 'o', 'e', 'z', 'e', 'b', ' ', 'd', 'i', 'g', '!', 'h', 'o', 's', 'v'};
  pivot(tekst + 7, tekst + 12, tekst + 9);
  print(tekst + 4, tekst + 15);
}

void pivot(char *b, char *e, char *p)
{
  char tmp;
  char *q = p, *r = p;

  while (b < q-- && p++ < e)
  {
    tmp = *q;
    *q = *p;
    *p = tmp;
  }
}

void print(char const *b, char const *const e)
{
  do
  {
    printf("%c", *b);
  } while (++b < e);
}