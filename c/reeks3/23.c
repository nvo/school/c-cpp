#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define THRESHOLD 1e-9

/*
Zorg dat je oefening 13 onder de knie hebt, voor je hieraan begint. In deze oefening wordt met
gehele getallen gewerkt in plaats van reele getallen.

1.  Schrijf een functie plaats_van(t, n, g), die een pointer teruggeeft naar het eerste element
    van een gegeven array t van n gehele getallen, naar de gegeven waarde g . Indien het getal
    niet aanwezig is, wordt de nullpointer teruggegeven.

2.  Schrijf een hoofdprogramma om je functie uit te testen. Probeer alle randgevallen: zoek
    het eerste getal, het laatste getal, en een getal dat niet in de array voorkomt.
    Breid je hoofdprogramma uit: verander een gevonden getal in zijn tweevoud, door eerst de
    functie plaats_van op te roepen en dan het gevonden getal (in de array!) te verdubbelen. Je
    zal eventueel de types van de parameter en het returntype van de functie plaats_van(...)
    moeten aanpassen! Schrijf ter controle de hele array uit. (Wat zou er gebeuren als je een
    getal dat niet aanwezig is in de array wil verdubbelen? Voorziet jouw code dat er dan een
    foutboodschap komt in plaats van een crash?)

3.  Schrijf een procedure plaats_ptr_op_getal(p, n, g), die een gegeven pointer p verplaatst
    naar de plaats waarop een gegeven geheel getal g in een gegeven array van n gehele
    getallen gevonden wordt. Indien het getal niet aanwezig is, wordt de gegeven pointer de
    nullpointer. Gebruik de functie plaats_van niet; gebruik schuivende pointers in plaats van
    indexering.

4.  Breid je hoofdprogramma uit: zoek via de procedure plaats_ptr_op_getal een getal in de
    array. Print de (rest van de) array vanaf het gevonden getal uit. Hou er opnieuw rekening
    mee dat het getal eventueel niet aanwezig is in de array.
*/

double *index_of(double[], int, double);
void set_ptr(double **, int, double);

int main(int argc, char *argv[])
{
  double arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  double x;

  int n = sizeof(arr) / sizeof(double);

  // INDEX_OF
  printf("INDEX_OF:\n");
  do
  {
    fflush(stdin);
    printf("Enter number: ");
  } while (scanf("%lf", &x) <= 0);

  double *p = index_of(arr, n, x);
  if (p)
  {
    printf("pointer to %.2f: %p (%.2lf)\n", x, p, *p);
    *p *= 2;
  }
  else
    printf("%.2f not present in array!\n", x);

  // SET_PTR
  printf("\nSET_PTR:\n");
  do
  {
    fflush(stdin);
    printf("Enter number: ");
  } while (scanf("%lf", &x) <= 0);

  int i;
  for (i = 0; i < n; i++)
    printf("%.2lf ", arr[i]);
  printf("\n");

  double *q = arr;
  set_ptr(&q, n, x);
  if (q)
  {
    for (i = q - arr; i < n; i++)
      printf("%.2lf ", arr[i]);
  }

  // free heap allocated memory
  free(p);
}

double *index_of(double t[], int n, double g)
{
  double *p = malloc(sizeof(double));
  p = t;

  int i;
  for (i = 1; i < n; i++, p++)
    if (fabs(*p - g) < THRESHOLD)
      return p;

  return NULL;
}

void set_ptr(double **p, int n, double g)
{
  int i;
  for (i = 0; i < n; i++, (*p)++)
    if (**p == g)
      return;
  *p = NULL;
}
