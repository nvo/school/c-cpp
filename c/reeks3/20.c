#include <stdio.h>

/*
Wat doet deze code? Verklaar.
void wissel(int a, int b){
  int hulp;
  printf(" Bij start van de wisselprocedure hebben we a=%d en b=%d.\n",a,b);
  hulp = a;
  a = b;
  b = hulp;
  printf(" Op het einde van de wisselprocedure hebben we a=%d en b=%d.\n",a,b);
}

int main(){
  int x, y;
  x = 5;
  y = 10;

  printf("Eerst hebben we x=%d en y=%d.\n",x,y);
  wissel(x,y);
  printf("Na de wissel hebben we x=%d en y=%d.\n",x,y);

  return 0;
}

Deze vraag beantwoord je eerst ZONDER de code uit te proberen. Daarna kan je jouw antwoord
controleren, door de code te kopieren, compileren en uit te voeren. Omdat het kopieren vanuit
een .pdf-bestand de kantlijnen niet behoudt, kan je kopieren vanuit een andere bron. (Zeker
nuttig als we je later langere code geven!) Op Ufora vind je een map met .tex-bestanden. Daar
haal je het juiste bestand op (opg_20_wissel.tex), en kopieer je het programma (te vinden
tussen \begin{verbatim} en \end{verbatim}) in een .c-bestand.

Pas de code aan zodat de procedure nu wel doet wat ze belooft. Aan het hoofdprogramma
verander je niets of zo weinig mogelijk.
*/

// void wissel(int a, int b)
void wissel(int *a, int *b)
{
  int hulp;
  printf(" Bij start van de wisselprocedure hebben we a=%d en b=%d.\n", *a, *b);
  hulp = *a;
  *a = *b;
  *b = hulp;
  printf(" Op het einde van de wisselprocedure hebben we a=%d en b=%d.\n", *a, *b);
}

int main()
{
  int x, y;
  x = 5;
  y = 10;

  printf("Eerst hebben we x=%d en y=%d.\n", x, y);
  // wissel(x, y);
  wissel(&x, &y);
  printf("Na de wissel hebben we x=%d en y=%d.\n", x, y);

  return 0;
}

// everything in C is 'pass by value', yada yada yada
