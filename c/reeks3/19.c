#include <stdio.h>

/*
Wat wordt er uitgeschreven? Uiteraard beantwoord je deze vraag zonder computer.

Bijvraag: hoe zou je de hardgecodeerde bovengrenzen (3 en 6) van de for-lussen kunnen vervangen
door zelf de grootte van de array's te bepalen?
*/

#include <stdio.h>
int main()
{
  int t[6] = {0, 10, 20, 30, 40, 50};
  int *pt[3];

  int i;
  for (i = 0; i < 3; i++)
    pt[i] = &t[2 * i];
  // pt = { &t[0], &t[2], &t[4] }

  pt[1]++;       // pt = { &t[0], &t[3], &t[4] }
  pt[2] = pt[1]; // pt = { &t[0], &t[3], &t[3] }
  *pt[1] += 1;   // t  = { 0, 10, 20, 31, 40, 50 }
  *pt[2] *= 2;   // t  = { 0, 10, 20, 62, 40, 50 }

  int **ppt = &pt[0]; // ppt = &pt[0]
  (*ppt)++;           // pt = { &t[1], &t[3], &t[3] }
  **ppt += 1;         // t  = { 0, 11, 20, 62, 40, 50 }

  // t  = { 0, 11, 20, 62, 40, 50 }
  for (i = 0; i < 6; i++)
    printf("%d ", t[i]);
  printf("\n");

  // pt = { &t[1], &t[3], &t[3] }
  // pt = { 11, 62, 62 }
  for (i = 0; i < 3; i++)
    printf("%d ", *pt[i]);
  printf("\n");

  return 0;
}