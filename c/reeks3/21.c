#include <stdio.h>

/*
Dit is een vervolg op oefening 12. Gegeven onderstaand hoofdprogramma (haal eerst het latexbestand
af van Minerva, zodat je geen code hoeft over te tikken).

#include <stdio.h>
int main(){
  char letters [] = {'p','o','r','g','o','e','d','o','i','e','o','k','i',':','a','-','t','('};
  const char *p = letters;

  schrijf_aantal(letters+3,4);
  printf("\n");
  schrijf(p+10,p+12);
}

1.  Kan je de lengte van de array opvragen via de pointer p?
    Ga na dat de geheugenruimte, ingenomen door een pointer altijd gelijk is (onafhankelijk
    van het type waarnaar de pointer verwijst).

2.  Schrijf de procedure schrijf_aantal die het aantal gevraagde letters uitschrijft vanaf de
    pointer opgegeven in de eerste parameter.

3.  Schrijf de procedure schrijf(begin,eind) die de letters uitschrijft die te vinden zijn tussen
    de plaats waar de pointers begin en eind naar wijzen. (Laatste grens niet inbegrepen; je
    mag er vanuit gaan dat beide pointers wijzen naar elementen in dezelfde array van karakters,
    en dat begin < eind .)
*/

void print_amount(char const *p, int n);
void print(char const *b, char const *const e);

int main()
{
  char letters[] = {'p', 'o', 'r', 'g', 'o', 'e', 'd', 'o', 'i', 'e', 'o', 'k', 'i', ':', 'a', '-', 't', '('};
  const char *p = letters;
  // sizeof van eender welke pointer is altijd even groot (4 byte), want dit verwijst gewoon naar een geheugenadres

  print_amount(letters + 3, 4);
  printf("\n");
  print(p + 10, p + 12);
}

void print_amount(char const *p, int n)
{
  int i;
  for (i = 0; i < n; i++)
    printf("%c", *p++);
}

void print(char const *b, char const *const e)
{
  do
  {
    printf("%c", *b);
  } while (++b < e);
}
