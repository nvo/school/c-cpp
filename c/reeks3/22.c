#include <stdio.h>

/*
- Schrijf een procedure zoek_extremen(t, n, min, max) die op zoek gaat naar het minimum
  én maximum in een array t van n gehele getallen. Het minimum, resp. maximum
  komt terecht in de parameter min resp. max .
  In het hoofdprogramma zorg je voor een hardgecodeerde array, en je schrijft het minimum
  en maximum uit. Test kritisch!

- Schrijf een recursieve versie van deze procedure.
*/

void find_extremes(int const *, int, int *, int *);
void find_extremes_rec(int const *, int, int *, int *);

int main(int argc, char *argv[])
{
  int t[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  int n = sizeof(t) / sizeof(int);
  int min, max;

  find_extremes((int const *)t, n, &min, &max);
  printf("min: %d\nmax: %d\n\n", min, max);

  int min_r, max_r;
  find_extremes_rec((int const *)t, n, &min_r, &max_r);
  printf("min: %d\nmax: %d\n", min_r, max_r);
}

void find_extremes(int const *t, int n, int *min, int *max)
{
  if (n < 1)
    return;

  *min = t[0];
  *max = t[0];

  int i;
  for (i = 1; i < n; i++)
  {
    if (*min > t[i])
      *min = t[i];
    if (*max < t[i])
      *max = t[i];
  }
}

void find_extremes_rec(int const *t, int n, int *min, int *max)
{
  if (n < 1)
    return;

  if (n < 2)
  {
    *min = t[0];
    *max = t[0];
  }

  find_extremes_rec(t, n - 1, min, max);
  if (*min > t[n - 1])
    *min = t[n - 1];
  if (*max < t[n - 1])
    *max = t[n - 1];
}
