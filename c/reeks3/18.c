#include <stdio.h>

/*
Beantwoord deze vraag zonder computer. Welke regels zijn syntactisch niet correct - en zullen
een compilerfout opleveren. Welke bewerkingen kloppen wel qua syntax, maar zijn zinloos en/of
gevaarlijk voor het verloop van het programma.

Wat zit er uiteindelijk in de variabelen? Regels die een compilefout geven, of die zinloos/gevaarlijk
zijn negeer je hierbij.
*/

int main()
{
  int i = 7, j;
  double d;
  int *ip, *jp, *tp;
  double *dp;
  void *v;
  const int *p1;
  int *const p2 = &i;
  int t[25] = {10, 20, 30, 40, 50, 60};
  /* 1*/ jp = &i;
  // OK:  jp = ->i

  /* 2*/ j = *jp;
  // OK:  j = 7

  /* 3*/ *ip = i;
  // OK:  als ip gedefinieerd is en ergens naar wijst!  *ip = 7
  //! NOK: als ip nog nergens naar verwijst => in dit geval

  /* 4*/ ip = jp;
  // OK:  ip = ->i

  /* 5*/ &i = ip;
  // NOK: compilation error

  /* 6*/ (*ip)++;
  // OK:  ip = ->i
  //      i = 8

  /* 7*/ *ip *= i;
  // OK:  ip = ->i
  //      i = 64

  /* 8*/ ip++;
  // NOK: ip verwijst naar het eerst volgende adres
  //      ! gevaarlijk, enkel gebruiken bij pointers naar arrays

  /* 9*/ tp = t + 2;
  // OK:  tp = ->t[2]

  /*10*/ j = &t[5] - tp;
  // OK:  j = 3

  /*11*/ t++;
  // NOK: arrays zijn constante pointers, de ++ operator zou de array overschrijven maar dit wordt niet toegelaten door de compiler

  /*12*/ (*t)++;
  // OK: t[0] = 11

  /*13*/ *tp--;
  // OK:  tp = ->t[1]
  //      t[1] = 21

  /*14*/ j = (*tp)++;
  // OK:  j = 21
  //      t[1] = 22

  /*15*/ i = *tp++;
  // OK:  i = 21
  //      tp = ->t[2]

  /*16*/ v = tp++;
  // OK:  v = ->t[2]
  //      tp = ->t[3]

  /*17*/ printf("%d", *v);
  // NOK: void pointers kunnen niet gederefereerd worden zonder gecast te worden naar een andere pointer

  /*18*/ v++;
  // NOK: de grootte van een void pointer is niet geweten, aangezien het incrementeren van een pointer rekening houdt met de grootte van het onderliggende datatype,
  //      kan de operatie niet worden uitgevoerd zonder eerst te casten naar een andere soort pointer.

  /*19*/ p1 = ip;
  // OK:  p1 = ->>i
  //      ! de waarde van i kan niet gewijzigd worden via deze pointer!

  /*20*/ jp = p1;
  // NOK: const pointer = pointer ==> BAD

  /*21*/ (*p1)--;
  // NOK: waarde van de const pointer kan niet worden gewijzigd

  /*22*/ dp = &i;
  // NOK: type interference!

  /*23*/ dp = v;
  //! OK: maar gevaarlijk, omzeilt *22
  //      dp = ->t[2]

  /*24*/ jp = p2;
  // OK:  jp = ->i

  /*25*/ p2 = p1;
  // NOK: *const pointer kan niet gewijzigd worden

  /*26*/ *p2 += i;
  // OK:  p2 = ->i
  //      i = 42

  return 0;
}
