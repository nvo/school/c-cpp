#include <stdio.h>

#define N 5

/*
Schrijf een programma dat aan de gebruiker vijf positieve gehele getallen vraagt. Geeft de
gebruiker echter een negatief getal in, dan stopt het programma met getallen opvragen. Nadien
schrijft het programma uit of de gebruiker inderdaad vijf positieve gehele getallen opgaf. Tot slot
wordt de som van de ingegeven positieve gehele getallen uitgeschreven (ook als er niet genoeg
ingegeven werden). Test uit, onder andere met de getallenreeks 1 2 3 4 -5.
*/

int main(int argc, char const *argv[])
{
    int sum = 0;
    printf("give 5 positive numbers: ");

    int i;
    for (i = 0; i < N; i++)
    {
        int x;
        scanf("%d", &x);
        if (x < 0)
            break;

        sum += x;
    }
    printf("Sum of positive numbers: %d\n", sum);

    return 0;
}
