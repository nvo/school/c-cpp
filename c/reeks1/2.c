#include <stdio.h>

#define N 64

/*
Schrijf een programma dat alle (gehele) getallen van 0 tot en met 64 uitschrijft. Per regel komt
zowel octale, decimale, als hexadecimale voorstelling van één getal. Zorg ervoor dat de getallen
rechts gealligneerd zijn, dus zo:

  0       0       0
  1       1       1
  2       2       2
  3       3       3
...
 11       9       9
 12      10       a
 13      11       b
 14      12       c
 15      13       d
 16      14       e
 17      15       f
 20      16      10
 21      17      11
...
 77      63      3f
100      64      40
*/

int main(int argc, char const *argv[])
{
  int i;
  for (i = 0; i <= N; i++)
  {
    printf("%3o\t%3d\t%3x\n", i, i, i);
  }

  return 0;
}
