#include <stdio.h>

/*
Deze code levert, in tegenstelling tot vorige oefening, wél de gevraagde output. Toch levert deze
oplossing nog minder punten op dan de vorige oplossing. Waarom?
*/

int main(int argc, char const *argv[])
{
    int macht = 1;
    int i;
    for (i = 0; i < 20; i++) // for loop gebruikt ookal ben je niet zeker hoe vaak je de loop moet uitvoeren
    {
        printf("%d ", macht);
        macht *= 2;
        if (macht > 10000) // deze if wegnemen door de conditie in een while te plaatsen
        {
            break;
        }
    }
    return 0;
}
