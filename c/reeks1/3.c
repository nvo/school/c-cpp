#include <stdio.h>

/*
Gegeven volgend programmafragment, als oplossing voor oefening 1. Dit levert de gevraagde
output. Het levert echter op een examen strafpunten op. Waarom?
*/

int main(int argc, char const *argv[])
{
    int i;
    for (i = 10; i > 0; i--)
    {
        if (i == 10)
        {
            // deze if kan buiten de for loop gebracht worden
            printf("Hello world!\n");
        }
        printf("%d ", i);
        if (i == 1)
        {
            // deze if kan buiten de for loop gebracht worden
            printf("\nSTART");
        }
    }
    return 0;
}
