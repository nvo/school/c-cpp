#include <stdio.h>

/*
Om de grootste gemende deler (ggd) van twee getallen te berekenen, werd je allicht aangeleerd om
de twee getallen eerst te ontbinden in priemfactoren, om dan de gemeenschappelijke factoren te
ontdekken. Het kan ook anders, met het algoritme van Euclides dat gebruik maakt van volgende
gelijkheid:

ggd(a , b) = ggd(b , a mod b);

De uitdrukking a mod b lees je als ‘a modulo b’ en stelt de rest van a bij deling door b voor. In
C (en C++ ) gebruik je de notatie % in plaats van mod. Het algoritme van Euclides vervangt de
getallen a en b (herhaaldelijk) door de getallen b en a mod b. Indien a>b, zullen b en a mod b
kleiner zijn dan a en b - en dus werd het probleem vereenvoudigd. Dit vervangen stopt van zodra
een van de getallen 0 is: ggd(a,0) = a.

Schrijf een recursieve functie ggd(a,b) die de grootste gemene deler van twee gehele getallen
berekent.

Test uit met
ggd(-6,-8)==2
ggd(24,18)==6
ggd( 0,-5)==5
ggd(6,-35)==1
*/

int gcd(int, int);

int main(int argc, char const *argv[])
{
    printf("%-20s%3d\n%-20s%3d\n%-20s%3d\n%-20s%3d\n",
           "gcd(-6, -8) == 2?", gcd(-6, -8),
           "gcd(24, 18) == 6?", gcd(24, 18),
           "gcd( 0, -5) == 5?", gcd(0, -5),
           "gcd(6, -35) == 1?", gcd(6, -35));
    return 0;
}

int gcd(int a, int b)
{
    if (a == 0)
        return b < 0 ? -b : b;
    if (b == 0)
        return a < 0 ? -a : a;

    return gcd(b, a % b);
}
