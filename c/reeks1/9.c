#include <stdio.h>

/*
1.
Schrijf een functie cijfersom(x) die van een gegeven geheel getal x de som van de cijfers
berekent. Zo is cijfersom(12345) gelijk aan 15.

2.
Maak een recursieve versie cijfersom_rec(x) die hetzelfde doet als cijfersom(x).
*/

int numbersum(int);
int numbersum_rec(int);

int main(int argc, char const *argv[])
{
    int x = 12345;

    printf("%-16s%d\n", "normal func:", numbersum(x));
    printf("%-16s%d\n", "recursive func:", numbersum_rec(x));

    return 0;
}

int numbersum(int x)
{
    int s = 0;

    do
        s += x % 10;
    while ((x /= 10) > 0);

    return s;
}

int numbersum_rec(int x)
{
    if (x < 10 && x > -10)
        return x;
    return x % 10 + numbersum_rec(x / 10);
}
