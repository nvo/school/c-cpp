#include <stdio.h>
#define N 100

/*
Schrijf een programma dat volgende tekst op het scherm brengt (delay bij uitschrijven van de
verschillende getallen is niet nodig).
Hello world!
10 9 8 7 6 5 4 3 2 1
START

Opmerkingen:
• Heb je de output 10 9 8 7 6 5 4 3 2 1 hardgecodeerd? Niet doen!
• Kan je het aftellen laten beginnen op 100? Gebruik een constante voor de startwaarde van het aftellen.
*/

int main(int argc, char const *argv[])
{
    printf("Hello World!\n");

    int i;
    for (i = N; i >= 0; i--)
    {
        printf("%d ", i);
    }
    printf("\n");

    printf("START\n");
    return 0;
}
