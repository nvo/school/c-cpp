#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MIN 2
#define MAX 20

/*
In deze opgave wordt de faculteit van positieve gehele getallen berekend.
1.  Bereken de faculteit van de gehele getallen 2 tot en met 30 en schrijf de resultaten uit. Wat
    merk je op?

2.  Bereken de faculteit met andere gegevenstypes (geheel én reëel). Hoe exact is de berekende
    waarde voor de faculteit van grotere getallen?

3.  Pas het programma aan zodat de computer een willekeurig geheel getal kiest tussen 2 en
    20 (grenzen inbegrepen). Bereken de faculteit van dit getal.
    Gebruik de functie rand(); - meer informatie zoek je online.

4.  Pas het programma aan zodat de gebruiker een positief geheel getal kan ingeven tussen 2 en
    20 (grenzen inbegrepen). Controleer deze input! Vraag (desnoods herhaaldelijk) een nieuw
    getal indien de gebruiker geen geheel getal ingaf of indien het ingegeven geheel getal niet
    tot het interval [2,20] behoort. Vergeet niet om na elke foutieve input de resterende tekst
    uit de inputbuffer te verwijderen. Bereken de faculteit van dit getal.
*/

long double factorial(int);

int main(int argc, char const *argv[])
{
    // 1
    long double x = 1; // 2
    printf("0:\t1\n");
    printf("1:\t1\n");

    int i;
    for (i = 2; i <= 30; i++)
    {
        x *= i;
        printf("%d:\t%.0Lf\n", i, x);
    }
    printf("\n\n");

    // 3
    time_t t;
    srand((unsigned)time(&t));

    int a = rand() % (MAX + 1 - MIN) + MIN;
    printf("%d:\t%.0Lf\n", a, factorial(a));

    // 4
    printf("Enter a number: ");
    while (scanf("%d", &a) <= 0 || a < MIN || a > MAX)
    {
        while (getchar() != '\n')
            ;
        printf("Not a valid number! Choose a number between %d and %d.\n", MIN, MAX);
        printf("Enter a number: ");
    }
    fflush(stdin);
    printf("%d:\t%.0Lf\n", a, factorial(a));

    return 0;
}

long double factorial(int x)
{
    if (x < 0)
        return -1;
    if (x < 2)
        return 1;

    long double y = x;
    int i;
    for (i = x - 1; i >= 2; i--)
    {
        y *= i;
    }
    return y;
}
