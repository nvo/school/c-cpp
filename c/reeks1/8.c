#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define N 10
#define PRECISION 0.0000001
#define MIN -3.200
#define MAX 3.200
#define RAND_PRECISION 1000

/*
Bij het berekenen van de sinus van een gegeven hoek in radialen, gebruikt je computer of reken-
machine onderstaande reeksontwikkeling.
sin(x) = SUM(n=0, ∞) { (-1)^n / (2n + 1)! * x^(2n + 1) }
*/

double my_sin(double x);

int main(int argc, char const *argv[])
{
    double x = 8.2, sinx = x, stl_sinx = sin(x), t = x;

    // 1
    int n;
    for (n = 1; n < N; n++)
    {
        t *= ((-1) * x * x) / (2 * n * (2 * n + 1));
        sinx += t;
    }
    printf("impl.:\t%f\nstl.:\t%f\n", sinx, stl_sinx);

    // 2
    sinx = x;
    t = x;
    n = 1;

    while (fabs(stl_sinx - sinx) > PRECISION)
    {
        t *= ((-1) * x * x) / (2 * n * (2 * n + 1));
        sinx += t;
        n++;
    }
    printf("impl.:\t%f\nstl.:\t%f\n", sinx, stl_sinx);

    // 3
    time_t now;
    srand((unsigned)time(&now));

    x = (double)(rand() % (int)((MAX - MIN) * RAND_PRECISION) + (int)(MIN * RAND_PRECISION)) / RAND_PRECISION;
    stl_sinx = sin(x);

    sinx = x;
    t = x;
    n = 1;

    printf("sin(%.3f):\n", x);
    while (fabs(stl_sinx - sinx) > PRECISION)
    {
        t *= ((-1) * x * x) / (2 * n * (2 * n + 1));
        sinx += t;
        n++;
    }
    printf("impl.:\t%f\nstl.:\t%f\n", sinx, stl_sinx);

    return 0;
}

double my_sin(double x)
{
}
