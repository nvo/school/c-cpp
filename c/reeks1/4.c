#include <stdio.h>

/*
Gegeven de opgave schrijf alle machten van 2 (beginnend bij 2 0 = 1), kleiner dan 10.000 uit.
De onderstaande code geeft niet de gevraagde uitvoer - hoe kan je dit heel snel detecteren?
Pas de code ook aan zodat je de gevraagde uitvoer bekomt.
*/

int main(int argc, char const *argv[])
{
    int macht = 1;
    while (macht < 10000)
    {
        // origineel
        // macht *= 2;
        // printf("%d ", macht);

        // aangepast
        printf("%d ", macht);
        macht *= 2;
    }
    return 0;
}
