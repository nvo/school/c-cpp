#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MIN 2
#define MAX 20

/*
Herneem oefening 6, maar los dit nu op met twee functies:

1.
De functie faculteit(x) berekent de faculteit van een gegeven geheel getal x.

2.
De functie faculteit_rec(x) doet een recursieve berekening.
*/

long double factorial(int);
long double factorial_rec(int);

int main(int argc, char const *argv[])
{
    // 1
    long double x = 1; // 2
    printf("0:\t1\n1:\t1\n");

    int i;
    for (i = 2; i <= 30; i++)
    {
        x *= i;
        printf("%d:\t%.0Lf\n", i, x);
    }
    printf("\n\n");

    // 3
    time_t t;
    srand((unsigned)time(&t));

    int a = rand() % (MAX + 1 - MIN) + MIN;
    printf("%d:\t%.0Lf\t%.0Lf\n", a, factorial(a), factorial_rec(a));

    // 4
    printf("Enter a number: ");
    while (scanf("%d", &a) <= 0 || a < MIN || a > MAX)
    {
        while (getchar() != '\n')
            ;
        printf("Not a valid number! Choose a number between %d and %d.\n", MIN, MAX);
        printf("Enter a number: ");
    }
    fflush(stdin);
    printf("%d:\t%.0Lf\t%.0Lf\n", a, factorial(a), factorial_rec(a));

    return 0;
}

long double factorial(int x)
{
    if (x < 0)
        return 0;
    if (x < 2)
        return 1;

    long double y = x;
    int i;
    for (i = x - 1; i >= 2; i--)
    {
        y *= i;
    }
    return y;
}

long double factorial_rec(int x)
{
    if (x < 0)
        return 0;
    if (x < 2)
        return 1;

    return x * factorial_rec(x - 1);
}
