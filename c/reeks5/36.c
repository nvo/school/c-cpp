#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
Bij het berekenen van de afstand tussen twee punten wordt doorgaans gebruikgemaakt van
de Euclidische afstand (zie https://nl.wikipedia.org/wiki/Afstand). Aangezien de Euclidische
afstand eigenlijk de afstand in vogelvlucht is, is deze afstand bij enkele praktische problemen
niet de meest bruikbare. Denk bijvoorbeeld aan het wegennet waarbij de afstand in vogelvlucht
niet noodzakelijk de kortste afstand hoeft te zijn. Bij deze vraag gaan we - naast de euclidische
afstand - twee andere manieren beschouwen om de afstand tussen twee punten te berekenen.

De Manhattan-afstand (zie https://nl.wikipedia.org/wiki/Manhattan-metriek ) dankt zijn naam
aan het typische stratenpatroon van Manhattan waar alle straten ofwel loodrecht op elkaar ofwel
evenwijdig aan elkaar lopen. In dit geval wordt de afstand gegeven als de som van het verschil
van de x-coordinaten en het verschil van de y-coordinaten. Dit komt overeen met de weg die je
zou afleggen om van het ene punt naar het andere punt te gaan als je enkel evenwijdig met de
x-as en de y-as zou mogen wandelen.

De Chebyshev-afstand (zie https://en.wikipedia.org/wiki/Chebyshev distance) of de schaakbordafstand
dankt zijn naam aan het feit dat op een discreet rooster deze afstand overeenkomt met
het aantal zetten dat de koning uit het schaakspel nodig heeft om van het ene punt naar het
andere punt te gaan. De afstand wordt in dit geval gegeven door het maximum van het verschil
van de x-coordinaten en het verschil van de y-coordinaten.

Voorzie het volgende:

1.  Denieer een struct met als naam punt en twee attributen van het type double voor het
    bijhouden van de x- en de y-coordinaat.

2.  Schrijf drie functies die respectievelijk de Euclidische, de Manhattan en schaakbordafstand
    berekenen. Elk van deze functies geeft de afstand als reëel getal terug.

3.  Schrijf een functie bepaal_max_afstand(punten, aantal, functie) die van een gegeven
    array met punten de maximum afstand bepaalt. De functie waarmee de afstand moet
    worden berekend, wordt als derde parameter meegeven. De maximumafstand wordt als
    reëel getal teruggegeven.

4.  Schrijf een hoofdprogramma dat aan de gebruiker vraagt hoeveel punten er zullen worden
    ingegeven. Het programma leest exact het aantal opgegeven punten en stopt die in een
    array die net voldoende plaats heeft om alle punten bij te houden. Het inlezen gebeurt
    punt per punt en dat volgens het patroon x , y. De gebruiker geeft dus de x-coordinaat
    gevolgd door een spatie gevolgd door een komma gevolgd door een spatie en gevolgd door
    de y-coordinaat. Het programma schrijft vervolgens de maximum Euclidische, maximum
    Manhattan en maximum schaakbordafstand naar het scherm.
*/

typedef struct
{
  double x, y;
} vector;

double euclidian_dist(vector const *, vector const *);
double manhattan_dist(vector const *, vector const *);
double chebyshev_dist(vector const *, vector const *);

double max_dist(vector const *, int, double (*)(vector const *, vector const *));

int main()
{
  int n, i;
  printf("Aantal punten: ");
  scanf("%d", &n);

  vector *v = malloc(sizeof(vector) * n);
  for (i = 0; i < n; i++)
    scanf("%lf , %lf", &v[i].x, &v[i].y);

  printf("max euclidian: %.2lf\nmax manhattan: %.2lf\nmax chebyshev: %.2lf\n",
         max_dist((vector const *)v, n, euclidian_dist),
         max_dist((vector const *)v, n, manhattan_dist),
         max_dist((vector const *)v, n, chebyshev_dist));

  free(v);
}

double max_dist(vector const *v, int n, double (*dist)(vector const *, vector const *))
{
  if (n < 2)
    return -1;

  double d, max = -1;
  int i, j;
  for (i = 0; i < n - 1; i++)
    for (j = i + 1; j < n; j++)
      if ((d = dist(&v[i], &v[j])) > max)
        max = d;
  return d;
}

double euclidian_dist(vector const *u, vector const *v)
{
  return sqrt((v->x - u->x) * (v->x - u->x) + (v->y - u->y) * (v->y - u->y));
}

double manhattan_dist(vector const *u, vector const *v)
{
  return fabs(u->x - v->x + u->y - v->y);
}

double chebyshev_dist(vector const *u, vector const *v)
{
  return fmax(fabs(u->x - v->x), fabs(u->y - v->y));
}
