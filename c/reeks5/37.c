#include <stdio.h>
#include <stdlib.h>

/*
Loop niet te snel in deze oefeningen, volg de opdracht nauwgezet. (Dus geen functies/procedures
waar het niet staat.)

1.  Denieer een struct Deeltal met drie velden: waarde, aantal_delers en delers. De
    waarde zal een geheel getal bevatten; aantal_delers geeft aan hoeveel delers (tussen 1 en
    waarde-1) dit getal heeft; delers is een pointer naar int(s), hier zullen de delers te vinden
    zijn.

2.  Werk dit deel uit in het hoofdprogramma.
    Maak lokaal een variabele van het type Deeltal aan. Vul op de volgende regels de drie
    velden van deze variabele in: waarde wordt 6, aantal_delers wordt 3 en de drie delers
    zijn 1, 2 en 3. Gebruik hiervoor geen lus.

3.  Schrijf een procedure schrijf_ints(...) die een array van ints meekrijgt, en deze gehele
    getallen naast elkaar uitschrijft met een liggend streepje tussen.

4.  Gebruik de bovenstaande procedure in de procedure schrijf_deeltal(d) die een deeltal
    (=getal en zijn delers) uitschrijft. Voor het getal 6 zou er komen:
      6 1-2-3
    Heb je in de schrijf-procedure(s) een kopie gemaakt, of ben je zuiniger geweest?

5.  Schrijf de functie aantal_delers_van(x) die telt hoeveel delers het geheel getal x heeft.
    (Overloop hiervoor alle getallen van 1 tot en met x/2.)
6.  Schrijf een functie delers_van(x,aantal) die een pointer teruggeeft naar een array die
    alle delers bevat van het geheel getal x. De parameter aantal bevat het aantal delers van
    x. Test uit door in je hoofdprogramma de delers van 6 niet handmatig in te geven.

7.  Schrijf een procedure lees_deeltal(g) die de velden van een deeltal invult: het veld
    waarde wordt hierbij ingelezen vanop het toetsenbord; de andere velden worden automatisch
    berekend (gebruik de voorgaande functies).

8.  Schrijf een procedure lees_deeltallen(t,aantal) die een aantal deeltallen inleest en
    bijhoudt in de array t. Gebruik bovenstaande procedure (uiteraard).

9.  Schrijf een procedure schrijf_deeltallen(t,aantal) die een array t van deeltallen uitschrijft.
    Overloop de array met indexering, en gebruikt een hulpprocedure om elk deeltal
    apart uit te schrijven.

10. Test voorgaande procedures uit in een hoofdprogramma: lees een deeltal in en schrijf het
    uit. Vraag de gebruiker nadien om een aantal op te geven, waarna je hem/haar exact zoveel
    deeltallen vraagt. Deze schrijf je ook uit.

11. Schrijf een functie zoek(waarde, t, aantal) die een pointer teruggeeft naar het deeltal
    met waarde waarde (type int) in de array t van deeltallen. (Wat geef je terug indien het
    gezochte niet aanwezig is?) Denk na over de types in de parameterlijst.

12. Schrijf een of meerdere procedures die memory-leaks voorkomen. Aantal en aard van de
    parameters bepaal je zelf. Tip: voor je de geheugenplaats van een deeltal weer vrijgeeft
    aan het geheugen, schrijf je de waarde van dat deeltal uit. Zo kan je zelf nakijken of alle
    deeltallen zijn vrijgegeven.
*/

typedef struct
{
  int waarde, aantal_delers, *delers;
} deeltal;

int aantal_delers_van(int);
int *delers_van(int, int *);

void schrijf_ints(int const *, int);
void schrijf_deeltal(deeltal const *);
void schrijf_deeltallen(deeltal const *, int);

void lees_deeltal(deeltal *);
void lees_deeltallen(deeltal *, int);

deeltal const *zoek(int, deeltal const *, int);

void free_pointers(deeltal *, int);

int main()
{
  int n;
  do
  {
    printf("Hoeveel deeltallen? ");
  } while (scanf("%d", &n) <= 0);

  deeltal *d = malloc(sizeof(deeltal) * n);

  lees_deeltallen(d, n);
  schrijf_deeltallen(d, n);

  deeltal const *s = zoek(n, d, n);
  schrijf_deeltal(s);

  free_pointers(d, n);
}

void free_pointers(deeltal *d, int n)
{
  printf("freeing memory...\n");

  int i;
  for (i = 0; i < n; i++)
  {
    schrijf_deeltal(&d[i]);
    free(d[i].delers);
  }
  free(d);
}

deeltal const *zoek(int x, deeltal const *t, int n)
{
  int i;
  for (i = 0; i < n; i++)
    if (t[i].waarde == x)
      return &t[i];
  return NULL;
}

void lees_deeltallen(deeltal *t, int n)
{
  int i;
  for (i = 0; i < n; i++)
    lees_deeltal(&t[i]);
}

void lees_deeltal(deeltal *g)
{
  do
  {
    printf("Enter number: ");
  } while (scanf("%d", &g->waarde) <= 0);
  fflush(stdin);

  g->delers = delers_van(g->waarde, &g->aantal_delers);
}

int *delers_van(int x, int *aantal)
{
  *aantal = aantal_delers_van(x);

  int *delers = malloc(sizeof(int) * *aantal);
  delers[0] = 1;

  int i, j = 1;
  for (i = 2; j < *aantal; i++)
    if (x % i == 0)
      delers[j++] = i;

  return delers;
}

int aantal_delers_van(int x)
{
  int i, n = 1;
  for (i = 2; i << 1 <= x; i++)
    if (x % i == 0)
      n++;
  return n;
}

void schrijf_deeltallen(deeltal const *t, int n)
{
  int i;
  for (i = 0; i < n; i++)
    schrijf_deeltal(&t[i]);
}

void schrijf_deeltal(deeltal const *d)
{
  printf("%d\t", d->waarde);
  schrijf_ints(d->delers, d->aantal_delers);
  printf("\n");
}

void schrijf_ints(int const *t, int n)
{
  int i;

  printf("%d", t[0]);
  for (i = 1; i < n; i++)
    printf("%c%d", '-', t[i]);
}
