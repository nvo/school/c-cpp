#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define AANTAL_WOORDEN 10
#define GEMIDDELDE_LENGTE_WOORDEN 7
#define TOTALE_LENGTE_ARRAY (AANTAL_WOORDEN * (GEMIDDELDE_LENGTE_WOORDEN + 1)) /*gebruik vorige constanten om dit te berekenen */

/*
In deze oefening lezen we een aantal (nl. 10) woorden in, en slaan deze op in één lange array
van karaktertekens. Het hoofdprogramma is deels gegeven (haal eerst het latex-bestand af van
Ufora, zodat je geen code hoeft over te tikken):

We kunnen toch aan alle woorden apart, als we (zie schets hieronder):
  1.  elk woord netjes afsluiten met een nullkarakter (dat zal automatisch gebeuren als we
      scanf(...) juist gebruiken), EN
  2.  de start van elk woord onthouden door er een pointer naar te laten wijzen, E
  3.  toegang hebben tot de onderste tabel uit de schets (toegang tot de bovenste tabel is dan
      niet meer nodig).

Nog één opmerking: de onderste tabel heeft als laatste element een nullpointer. Dat zal ervoor
zorgen dat de schrijfprocedure niet per se moet weten hoe lang de uit te schrijven (pointer)array
is.

Schrijf de procedures lees(...) en schrijf(...).

Ga voorlopig nog niet na of de gebruiker geen woorden opgeeft die te lang zijn.

Extra
Er werd nog niet expliciet nagegaan of de gebruiker geen woorden opgeeft die te lang zijn. Dit
kan je op twee manieren inbouwen.

  1.  Leg bij elk woord dat je inleest dezelfde beperking op; lees bijvoorbeeld niet meer dan
      GEMIDDELDE_LENGTE_WOORDEN in.
  2.  Leg bij elk woord dat je inleest een variabele beperking op; namelijk het aantal elementen
      dat nog vrij is in de array met lettertekens.

De eerste optie heeft het nadeel dat je allicht veel 'overschot' (niet-gebruikte elementen) hebt
in de array t. De tweede optie heeft het nadeel dat de gebruiker als eerste woord een heel lang
woord kan ingeven - waarna er geen plaats meer is voor de volgende. Pas de oefening daarom
als volgt aan, gebruik makend van de tweede manier om in te lezen:

Schrijf een programma dat woorden inleest, tot de gegeven array t van lengte LENGTE_ARRAY_T
vol is (het max aantal worden ligt dus niet vast). Laat de gebruiker telkens weten hoe lang het op
te geven woord maximaal mag zijn. Geeft de gebruiker een woord op waardoor de tabel overvol
raakt, dan zorg je ervoor dat enkel het eerste deel van dat woord bewaard wordt.
*/

void lees(char **pt);
void schrijf(char **pt);

int main()
{
  char *pt[AANTAL_WOORDEN + 1]; /* zodat je ook nog een nullpointer kan toevoegen op het einde van de pointertabel */

  char t[TOTALE_LENGTE_ARRAY];

  pt[0] = t;

  printf("Geef %d woorden in:\n", AANTAL_WOORDEN);
  lees(pt);    /* leest alle woorden in */
  schrijf(pt); /* schrijft alle woorden onder elkaar uit */

  return 0;
}

void lees(char **pt)
{
  int i, n;
  for (i = 0; i < AANTAL_WOORDEN; i++)
  {
    scanf("%s", pt[i]);
    n = strlen(pt[i]);
    pt[i + 1] = pt[i] + n + 1;
  }
  pt[AANTAL_WOORDEN] = NULL;
}

void schrijf(char **pt)
{
  while (*pt)
    printf("%s\n", *pt++);
}
