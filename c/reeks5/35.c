#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define M 6
#define N 1000

/*
Werk verder op vorige oefening.

Gegeven in een hoofdprogramma een array bestaande uit een ongekend aantal C-strings. De
array is afgesloten met een nullpointer. Gegeven een pointer p die wijst naar het eerste element
van deze array.

Schrijf een procedure plaats_ptr_op_string(...p, char c) die de pointer p verplaatst naar
de plaats in de gegeven array waar een C-string staat die begint met het karakter c. Indien deze
C-string niet aanwezig is, wordt p de null-pointer.

Test deze procedure uit in een hoofdprogramma. Je kan hiervoor uiteraard gebruik maken van
de voorgaande functie lees_meerdere().
*/

char *read();
char **read_multiple();

void set_ptr_to_string(char const ***, char);

int main()
{
  char **txt = read_multiple();

  char c = 'a';
  char const **found = (char const **)txt;
  set_ptr_to_string(&found, c);

  if (*found)
    printf("Found: %s\n", **found);
  else
    printf("No string starting with '%c'\n", c);

  while (*txt)
    free(*txt++);
  free(txt);

  return 0;
}

void set_ptr_to_string(char const ***p, char c)
{
  while (**p)
  {
    if (***p == c)
      return;
    (*p)++;
  }
}

char **read_multiple()
{
  char **str;
  char *buffer[M];

  int n = 0;
  while (n < M && strcmp((buffer[n++] = read()), "STOP"))
    ;
  str = malloc(sizeof(char *) * n);

  int i;
  for (i = 0; i < n - 1; i++)
    str[i] = buffer[i];
  str[n - 1] = NULL;

  return str;
}

char *read()
{
  char *str;
  char buffer[N];

  printf("Enter text: ");

  int n = 0;
  while (n <= N && (buffer[n++] = getchar()) != '\n')
    ;
  str = malloc(sizeof(char) * n);

  int i;
  for (i = 0; i < n - 1; i++)
    str[i] = buffer[i];
  str[n - 1] = 0;

  fflush(stdin);

  return str;
}
