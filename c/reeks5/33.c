#include <stdio.h>
#include <stdlib.h>

#define N 1000

/*
1.  Schrijf een functie lees() die een lijn (bestaande uit 1 of meerdere woorden) inleest vanop
    het klavier, en een nieuwe C-string teruggeeft. Je weet niet hoe lang de tekst is, maar na
    oproep van de functie neemt de C-string niet meer geheugenplaats in dan strikt noodzakelijk.
    Indien de tekst langer is dan 1000 karaktertekens, breek je het af na het 1000e teken en kuis
    je de resterende tekst op. Test dit uit door de constante 1000 aan te passen - uiteraard.
    Zorg er ook voor dat op het einde van de resulterende C-string geen newline-karakter staat.

2.  Test je functie uit in het onderstaande (half-afgewerkte) hoofdprogramma (haal eerst het
    latex-bestand af van Ufora, zodat je geen code hoeft over te tikken).
    Vervolledig het hoofdprogramma met 1 regel code.
*/

char *read();

int main()
{
  int i;
  for (i = 0; i < 5; i++)
  {
    char *txt = read();
    printf("Ik las ***%s*** \n", txt);
    free(txt);
  }
  return 0;
}

char *read()
{
  char *str;
  char buffer[N];

  printf("Enter text: ");

  int n = 0;
  while (n <= N && (buffer[n++] = getchar()) != '\n')
    ;

  str = malloc(sizeof(char) * n);
  int i;
  for (i = 0; i < n - 1; i++)
    str[i] = buffer[i];
  str[n - 1] = 0;

  fflush(stdin);

  return str;
}
