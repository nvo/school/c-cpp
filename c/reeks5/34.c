#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define M 6
#define N 1000

/*
Werk verder op vorige oefening. Gebruik de functie lees() - enkele aanpassingen zijn echter
noodzakelijk.

1.  Schrijf een functie lees_meerdere() die maximaal 6 lijnen tekst inleest vanop het klavier,
    tot de gebruiker de regel STOP intikt. Alle lijnen tekst (behalve STOP) worden teruggegeven.
    Merk op: de functie vraagt hoogstens MAXAANTAL=6 lijnen tekst aan de gebruiker.
    Voorzie net voldoende plaats voor elke tekst, en net voldoende plaats voor alle teksten (dat
    zijn er maximaal 6). Na de laatste tekst bewaar je een nullpointer.

2.  Test je functie uit in een hoofdprogramma: lees in en schrijf alles weer uit.
*/

char *read();
char **read_multiple();

int main()
{
  char **txt = read_multiple();
  while (*txt)
  {
    printf("Ik las ***%s*** \n", *txt);
    free(*txt++);
  }

  free(txt);

  return 0;
}

char **read_multiple()
{
  char **str;
  char *buffer[M];

  int n = 0;
  while (n < M && strcmp((buffer[n++] = read()), "STOP"))
    ;
  str = malloc(sizeof(char *) * n);

  int i;
  for (i = 0; i < n - 1; i++)
    str[i] = buffer[i];
  str[n - 1] = NULL;

  return str;
}

char *read()
{
  char *str;
  char buffer[N];

  printf("Enter text: ");

  int n = 0;
  while (n <= N && (buffer[n++] = getchar()) != '\n')
    ;
  str = malloc(sizeof(char) * n);

  int i;
  for (i = 0; i < n - 1; i++)
    str[i] = buffer[i];
  str[n - 1] = 0;

  fflush(stdin);

  return str;
}
