#ifndef __H_CONTAINERS
#define __H_CONTAINERS

#include <iostream>
#include <vector>
#include <set>
#include <unordered_set>
#include <stack>
#include <map>
#include <memory>

template <typename T>
std::ostream &operator<<(std::ostream &out, std::vector<T> const &v)
{
  auto it = v.begin();
  out << "[ " << *it++;
  for (; it != v.end(); it++)
    out << ", " << *it;
  out << " ]";
  return out;
}

template <typename T>
std::ostream &operator<<(std::ostream &out, std::set<T> const &s)
{
  auto it = s.begin();
  out << "{ " << *it++;
  for (; it != s.end(); it++)
    out << ", " << *it;
  out << " }";
  return out;
}

template <typename T>
std::ostream &operator<<(std::ostream &out, std::unordered_set<T> const &us)
{
  auto it = us.begin();
  out << "{ " << *it++;
  for (; it != us.end(); it++)
    out << ", " << *it;
  out << " }";
  return out;
}

template <typename T>
std::ostream &operator<<(std::ostream &out, std::stack<T> &st)
{
  out << std::endl;
  while (!st.empty())
  {
    out << st.top() << std::endl;
    st.pop();
  }
  return out;
}

template <typename K, typename V>
std::ostream &operator<<(std::ostream &out, std::map<K, V> const &m)
{
  auto it = m.begin();
  out << it->first << " -> " << it++->second;
  for (; it != m.end(); it++)
    out << std::endl
        << it->first << " -> " << it->second;
  return out;
}

template <typename T>
std::ostream &operator<<(std::ostream &out, std::unique_ptr<T> const &ptr)
{
  out << *ptr;
  return out;
}

#endif
