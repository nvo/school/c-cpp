/*
Schrijf een C++-programma dat van alle (kleine) letters in het bestand lord.txt de frequenties
uitschrijft (hoofdletters moet je niet tellen).

Om de frequenties bij te houden gebruik je een gewone array (nog geen vector);

Om het bestand in te lezen, gebruik je de juiste streams (geen input redirection).

De bestanden die je nodig hebt staan in de map txt-bestanden op Ufora.
*/
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
  ifstream in;

  in.open("files/lord.txt");
  if (!in)
  {
    cerr << "Kon bestand niet openen!" << endl;
    return 1;
  }

  int freq[26] = {0};
  char c;
  while (!in.fail())
  {
    c = tolower(in.get());
    if (c >= 'a' && c <= 'z')
      freq[c - 'a']++;
  }

  for (int i = 0; i < 26; i++)
    cout << (char)('a' + i) << ": " << freq[i] << endl;

  if (!in.eof())
  {
    cerr << "Fout tijdens lezen van het bestand!" << endl;
    in.close();
    return 1;
  }

  in.close();
  return 0;
}
