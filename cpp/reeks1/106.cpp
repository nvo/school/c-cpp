/*
Schrijf een procedure schrijf(array,aantal,achterstevoren,tussenteken) die een array
van gehele getallen uitschrijft. De tweede parameter bevat het aantal elementen in de array,
de derde parameter bepaalt of het uitschrijven van achter naar voor dan wel op normale wijze
moet gebeuren. De laatste parameter geeft het karakterteken mee dat tussen twee opeenvolgende
getallen uitgeschreven wordt.

Declareer in het hoofdprogramma een array t met de getallen 1 3 5 7 9 11 13. Roep daarna
de procedure schrijf als volgt aan:

  schrijf(t,7);
  schrijf(t,7,true);
  schrijf(t,7,false,'-');
  schrijf(t,7,true,'-');

Dit zou de volgende output moeten produceren:
  1 3 5 7 9 11 13
  13 11 9 7 5 3 1
  1-3-5-7-9-11-13
  13-11-9-7-5-3-1
*/

#include <iostream>
#include <string>
using namespace std;

void schrijf(int const *, int, bool = false, char = ' ');

int main()
{
  int t[7] = {1, 3, 5, 7, 9, 11, 13};

  schrijf(t, 7);
  schrijf(t, 7, true);
  schrijf(t, 7, false, '-');
  schrijf(t, 7, true, '-');
}

void schrijf(int const *tab, int n, bool reverse, char delim)
{
  cout << tab[reverse ? n - 1 : 0];
  for (int i = 1; i < n; i++)
    cout << delim << tab[reverse ? n - 1 - i : i];
  cout << endl;
}
