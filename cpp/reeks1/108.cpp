/*
Gegeven 2 tekstbestanden, stationnetje.txt en paddestoel.txt. Maak een derde bestand,
mix.txt, waarin je de oneven regels van het eerste bestand laat afwisselen met de even regels
van het tweede bestand. (Let wel: er zitten dus regels tussen die je niet in de mix terug zal
vinden! Maak de mix zo lang als het kortste bestand.)
*/

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
  ifstream in1, in2;
  ofstream out;

  in1.open("files/stationnetje.txt");
  if (!in1.is_open())
  {
    cerr << "Kon bestand niet openen!" << endl;
    return 1;
  }

  in2.open("files/paddestoel.txt");
  if (!in2.is_open())
  {
    cerr << "Kon bestand niet openen!" << endl;
    in2.close();
    return 1;
  }

  out.open("mix.txt");
  if (!out.is_open())
  {
    cerr << "Kon bestand niet openen!" << endl;
    out.close();
    return 1;
  }

  int i = 0;
  string line;
  while (!(in1.fail() || in2.fail() || out.fail()))
  {
    getline(i % 2 ? in1 : in2, line);
    getline(i++ % 2 ? in2 : in1, line);
    out << line << endl;
  }

  if (!(in1.eof() || in2.eof()))
  {
    cerr << "Fout tijdens lezen/schrijven van de bestanden!" << endl;
    in1.close();
    in2.close();
    out.close();
    return 1;
  }

  in1.close();
  in2.close();
  out.close();
}
