/*
Hier komt de C++ -variant van oefening 2. Schrijf een programma dat alle (gehele) getallen van
0 tot en met 64 uitschrijft. Per regel komt zowel octale, decimale, als hexadecimale voorstelling
van een getal. Zorg ervoor dat de getallen rechts gealligneerd zijn. Dit kan aan de hand van de
manipulator setw(aantal) die ervoor zorgt dat de volgende operand van de uitschrijfoperator
<< een vaste breedte van aantal karakters inneemt. Te vinden in de bibliotheek iomanip.
*/

#include <iostream>
#include <iomanip>
using std::cout;
using std::endl;
using std::setw;

using std::dec;
using std::hex;
using std::oct;

const int N = 64;
const int W = 4;

int main()
{
  for (int i = 0; i <= N; i++)
    cout << setw(W) << oct << i << setw(W) << dec << i << setw(W) << hex << i << endl;
}
