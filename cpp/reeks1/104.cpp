/*
Gebruik in deze opgave een constante N die de maximale grootte van alle tabellen vastlegt.

  1.  Schrijf een functie genereer_string(len) die een random standaardstring genereert van
      lengte len (len is een gegeven geheel getal >= 0). De resulterende string bestaat dus uit
      een aaneenschakeling van len (al dan niet verschillende) random kleine letters.

  2.  Schrijf een procedure vul_array_met_strings(tab, n, len) die de array tab opvult met
      n random strings van lengte len. Je gebruikt hierbij uiteraard de voorgaande functie
      geneer_string. Je mag veronderstellen dat de array tab groot genoeg is (n < N).

  3.  Schrijf een procedure schrijf(tab,n) die de gegeven array tab (die n standaard strings
      bevat) naar het scherm schrijft.

  4.  Schrijf een procedure bepaal_min_en_max(tab, n, min, max) die in de gegeven array tab
      (die n standaardstrings bevat) op zoek gaat naar de alfabetisch kleinste en grootste string
      en deze respectievelijk opslaat in de parameter min en max.

  5.  Test voorgaande procedures uit in een hoofdprogramma.
      Laat de gebruiker eerst een geheel getal uit het interval [5 , 10] ingeven. Controleer deze
      input en vraag (desnoods herhaaldelijk) een nieuw getal indien de gebruiker geen geheel
      getal uit het interval [5 , 10] heeft ingegeven.
      Genereer vervolgens N strings van deze lengte. Schrijf deze strings uit en bepaal de alfabetisch
      kleinste en grootste string.

  6.  Schrijf een procedure splits_woorden(tab, aantal, zin) die de gegeven string zin opsplitst
      in woorden. De array tab wordt opgevuld met de gevonden woorden en het geheel
      getal aantal wordt ingevuld met het aantal woorden. Zorg er zelf voor dat er nooit meer
      dan N woorden worden toegevoegd.
      Het is niet de bedoeling om de zin letter per letter te overlopen. Je gebruikt hier string-
      functies.

  7.  Vul het hoofdprogramma aan zodat een zin wordt gevraagd aan de gebruiker. Bepaal het
      alfabetisch kleinste en grootste woord uit deze zin.
*/

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using std::cin;
using std::cout;
using std::endl;
using std::getline;
using std::string;

const int N = 8;
const int ALPHABET = 26;

string genereer_string(int);
void vul_array_met_strings(string *, int, int);
void schrijf(string *, int);
void bepaal_min_en_max(string *, int, string &, string &);
void splits_woorden(string *, int &, string const &);

int main()
{
  srand(time(NULL));

  int l;
  string str[N];

  do
  {
    fflush(stdin);
    cout << "Geef een getal [5-10]: ";
    cin >> l;
  } while (l < 5 || l > 10);
  getchar(); // laatste '\n' weghalen

  vul_array_met_strings(str, N, l);
  schrijf(str, N);

  string min, max;
  bepaal_min_en_max(str, N, min, max);
  cout << "MIN:\t" << min << endl
       << "MAX:\t" << max << endl;

  string zin;
  int aantal;
  cout << "Geef een zin: ";
  getline(cin, zin);

  splits_woorden(str, aantal, zin);
  schrijf(str, N);

  bepaal_min_en_max(str, N, min, max);
  cout << "MIN:\t" << min << endl
       << "MAX:\t" << max << endl;

  return 0;
}

string genereer_string(int len)
{
  string str = "";
  for (int i = 0; i < len; i++)
    str += 'a' + rand() % ALPHABET;
  return str;
}

void vul_array_met_strings(string *tab, int n, int len)
{
  for (int i = 0; i < n; i++)
    tab[i] = genereer_string(len);
}

void schrijf(string *tab, int n)
{
  for (int i = 0; i < n; i++)
    cout << tab[i] << endl;
}

void bepaal_min_en_max(string *tab, int n, string &min, string &max)
{
  if (n < 1)
    return;

  min = tab[0];
  max = tab[0];
  for (int i = 1; i < n; i++)
  {
    if (tab[i] < min)
      min = tab[i];
    if (tab[i] > max)
      max = tab[i];
  }
}

void splits_woorden(string *tab, int &aantal, string const &zin)
{
  int n = zin.length();
  int sp, off = 0;
  aantal = 0;

  while (aantal < N && off < n)
  {
    sp = zin.find(' ', off);
    tab[aantal++] = zin.substr(off, sp - off);
    off = sp + 1;
  }
}
