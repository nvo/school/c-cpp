/*
Stel dat je een karakterteken wil omzetten naar een string (van lengte 1). Argumenteer waarom
welk stukje code (niet) juist is.
*/
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

int main()
{
  char c = 'x';
  string s = "" + c; // ! cstring + char WERKT NIET! strcat gebruiken!
  cout << "karakter " << c << " omgezet: " << s << "." << endl;

  char k = 'y';
  string w = ""; // cstring wordt naar een string gecast
  w += k;        // voegt het karakter achteraan toe aan de string
  cout << "karakter " << k << " omgezet: " << w << "." << endl;
}
