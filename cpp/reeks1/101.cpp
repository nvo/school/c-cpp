/*
Herneem oefening 1, maar dan in C++: schrijf op het scherm

Hello world!
10 9 8 7 6 5 4 3 2 1
START
*/

#include <iostream>
using std::cout;
using std::endl;

int main()
{
  cout << "Hello World!" << endl;

  for (int i = 10; i >= 0; i--)
    cout << i << " ";

  cout << endl
       << "START" << endl;

  return 0;
}
