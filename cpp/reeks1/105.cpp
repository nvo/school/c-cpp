/*
Schrijf een hoofdprogramma waarin je alvast volgende code schrijft (je kan de code kopieeren
van Ufora):

  double getallen[5] = {5.5,7.7,2.2,9.9,9.8};
  string woorden[3] = {"geloof","hoop","de liefde"};
  cout << "Grootste getal is: "<< grootste(getallen,5) << endl;
  cout << "Het grootste woord is: " << grootste(woorden,3) << "." << endl;

1.  Schrijf een functie grootste(array,lengte) die uit een gegeven array van gegeven lengte
    het grootste element teruggeeft. Het type van de elementen die in de array bewaard worden,
    is niet gekend.

    Tip: Voeg een functie grootte(elt) toe. Deze functie bepaalt de grootte van een element,
    en moet gemplementeerd moeten worden voor elk type waarvoor je de functie grootste
    oproept. De grootte van een woord wordt gedenieerd als de lengte van het woord.

    Opmerking:
    De functie grootte(...) wordt niet meegeven aan de functie grootste(...,...): we
    vragen hier dus geen functiepointers.

2.  Maak in het hoofdprogramma een array van drie personen aan. Elke persoon is van type
    persoon (denieer zelf de struct), en onthoudt zowel zijn naam (een string) als zijn leeftijd
    (een int) en lengte (in meter, dus een double).

3.  Schrijf een procedure initialiseer(pers,naam,leeftijd,lengte) die de velden van een
    gegeven persoon initialiseert. Gebruik deze procedure om de drie personen in de array te
    initialiseren ( vb. Samuel is 12 jaar, lengte is 1m52 / Jente is 22 jaar, lengte is 1m81 / Idris
    is 42 jaar, lengte 1m73).

4.  Schrijf een procedure print(pers) die de gegevens van een persoon uitschrijft op het
    scherm. (Test uit door een van de personen uit de array uit te schrijven.)

5.  Schrijf tenslotte in het hoofdprogramma, de grootste persoon uit, als de grootte van een
    persoon bepaald wordt door zijn leeftijd. (Nadien pas je de code aan zodat de grootte van
    een persoon bepaald wordt door zijn/haar lengte respectievelijk de lengte van zijn/haar
    naam. Krijg je het verwachte resultaat?)
*/

#include <iostream>
#include <string>
using namespace std;

struct persoon
{
  string naam;
  int leeftijd;
  double lengte;
};

template <typename T>
T grootste(T *, int);

int grootte(double);
int grootte(string const &);
int grootte(persoon const &);

void initialiseer(persoon &, string const &, int, double);
void print(persoon const &);

int main()
{
  double getallen[5] = {5.5, 7.7, 2.2, 9.9, 9.8};
  string woorden[3] = {"geloof", "hoop", "de liefde"};

  persoon personen[3];
  initialiseer(personen[0], "Samuel", 12, 1.52);
  initialiseer(personen[1], "Jente", 22, 1.81);
  initialiseer(personen[2], "Idris", 42, 1.73);

  cout << "Grootste getal is: " << grootste(getallen, 5) << endl;
  cout << "Het grootste woord is: " << grootste(woorden, 3) << "." << endl;
  cout << "De grootste persoon is: ";
  print(grootste(personen, 3));
  cout << "." << endl;
}

template <typename T>
T grootste(T *tab, int lengte)
{
  T max = tab[0];
  for (int i = 1; i < lengte; i++)
    if (grootte(tab[i]) > grootte(max))
      max = tab[i];
  return max;
}

int grootte(double elt)
{
  return elt;
}

int grootte(string const &elt)
{
  return elt.length();
}

int grootte(persoon const &elt)
{
  return elt.leeftijd;
  // return elt.lengte;
  // return elt.naam.length();
}

void initialiseer(persoon &pers, string const &naam, int leeftijd, double lengte)
{
  pers.naam = naam;
  pers.leeftijd = leeftijd;
  pers.lengte = lengte;
}

void print(persoon const &pers)
{
  cout << pers.naam << "(" << pers.leeftijd << " jaar, lengte is " << pers.lengte << "m)";
}
