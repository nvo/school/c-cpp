/*
Gegeven onderstaande code.

Test uit wat het programma doet. Het laatste element is leeg, maar wordt nog steeds getoond
(dat is de bedoeling!).

Probleem: De code s[i] = s[i+1]; zal de string kopiëren. Dat moeten we vermijden, want
een string kan in principe heel groot zijn.

Opdracht: Schrijf twee nieuwe procedures, die bij het onderstaande hoofdprogramma horen.
We bewaren nu (unique) pointers in de array, zodat we bij het opschuiven van de elementen in
de array enkel pointers moeten verleggen, en geen kopieen maken.

Probeer ook eens het laatste element uit de array te \verwijderen"!
*/
#include <string>
#include <memory>
#include <iostream>

using namespace std;

void schrijf(const string *s, int aantal)
{
  cout << endl;
  for (int i = 0; i < aantal - 1; i++)
  {
    cout << s[i] << " - ";
  }
  cout << s[aantal - 1];
}

void verwijder(string *s, int aantal, int volgnr)
{
  for (int i = volgnr; i < aantal - 1; i++)
  {
    s[i] = s[i + 1];
  }
  s[aantal - 1] = ""; //laatste element leeg maken
}

void schrijf(const unique_ptr<string> &s)
{
  if (s)
    cout << *s;
  else
    cout << "NULL";
}

void schrijf(const unique_ptr<string> *s, int aantal)
{
  cout << endl;
  for (int i = 0; i < aantal - 1; i++)
  {
    schrijf(s[i]);
    cout << " - ";
  }
  schrijf(s[aantal - 1]);
}

void verwijder(unique_ptr<string> *s, int aantal, int volgnr)
{
  if (volgnr == aantal - 1)
    s[volgnr].reset();
  else
    for (int i = volgnr; i < aantal - 1; i++)
      s[i] = move(s[i + 1]);
}

int main()
{
  string namen[] = {"Rein", "Ada", "Eppo", "Pascal", "Ilse"};

  unique_ptr<string> pnamen[] = {make_unique<string>("Rein"), make_unique<string>("Ada"), make_unique<string>("Eppo"), make_unique<string>("Pascal"), make_unique<string>("Ilse")}; //vul zelf deze array aan met 5 unieke pointers
  schrijf(pnamen, 5);
  verwijder(pnamen, 5, 1);
  schrijf(pnamen, 5);
  return 0;
}