/*
Je hebt in vorige oefeningen de uitschrijfoperator << geïmplementeerd voor verschillende containers.
Zet deze code (zonder het main-programma) in de headerle met naam containers.h,
en includeer deze header in het nieuwe .cpp-bestand.

1.  Maak een stack aan. Vul de stack met de woorden "een", "twee", "drie" en schrijf de
    stack daarna twee maal uit.

2.  Maak een array aan van (oorspronkelijk lege) vectoren. De array heeft lengte AANTAL=5.
    De tweede vector vul je op met de woorden aap, noot, mies. Schrijf daarna de array uit.

3.  Maak een (lege) vector aan van vectoren. Maak dan AANTAL=5 vectoren die hun lengte
    van bij de declaratie kennen: de i-de vector heeft lengte i (de eerste vector in het rijtje
    heeft dus lengte 0). Elke vector is ook al meteen opgevuld: de i-de vector bevat de getallen
    10 20 ... 10*i. Schrijf daarna de grote vector van achter naar voor uit; ook de 5 kleine
    vectoren schrijf je omgekeerd uit. Neem na elk van de kleine vectoren een nieuwe lijn.
*/

#include <string>
#include <iostream>
#include "../containers.h"

using namespace std;

const int AANTAL = 5;

int main()
{
  // 1
  stack<string> st;
  for (auto &i : {"een", "twee", "drie"})
    st.push(i);
  cout << st << endl
       << st << endl;

  // 2
  vector<string> v[AANTAL];
  for (auto &i : {"aap", "noot", "mies"})
    v[1].push_back(i);

  // 3
  vector<vector<int>> vv(AANTAL);
  for (int i = 0; i < AANTAL; i++)
  {
    vv[i].resize(i);
    for (int j = 0; j < i; j++)
      vv[i][j] = 10 * j;
  }

  for (int i = vv.size() - 1; i >= 0; i--)
  {
    for (int j = vv[i].size() - 1; j >= 0; j--)
      cout << vv[i][j] << " ";
    cout << endl;
  }
}