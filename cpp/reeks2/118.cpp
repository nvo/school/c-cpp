/*
Dit is een vraag die ooit (onder licht andere bewoordingen) gebruikt werd voor een test. Gebruik
enkel pen en papier, en verbeter daarna met de gegeven oplossing. (Wees ZEER KRITISCH!
Dat doen wij ook op een test :-) )

Gegeven een vector v met volgende declaratie.

  vector<map<string, stack<set<string>>>> v(5);

1. Schrijf een procedure

    vul_in_i_de_map_stack_bij_sleutel_aan_met_set_van_drie_strings
      (vect,i,sleutel,str1,str2,str3)

Start je van een net-geïnitialiseerde vector, dan zal de oproep

    vul_in_i_de_map_stack_bij_sleutel_aan_met_set_van_drie_strings
      (v,1,"noot","do","re","mi")

ervoor zorgen dat de eerste map uit de vector het woord noot afbeeldt op een stack met
één verzameling in. Die verzameling bevat de woorden do, re en mi.

2. Schrijf een logische functie

    i_de_map_beeldt_woord_af_op_stack_waarvan_bovenste_set_dit_element_bevat
      (vect,i,woord,element)

Deze functie bepaalt of de map op index i in de vector vect het woord woord afbeeldt op
een stack, waarvan de bovenste set het woord element bevat.
*/

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <stack>
#include <set>

using namespace std;

void vul_in_i_de_map_stack_bij_sleutel_aan_met_set_van_drie_strings(vector<map<string, stack<set<string>>>> &v, int m, string const &key, string const &val1, string const &val2, string const &val3)
{
  set<string> s{val1, val2, val3};
  v[m - 1][key].push(move(s));
}

bool i_de_map_beeldt_woord_af_op_stack_waarvan_bovenste_set_dit_element_bevat(vector<map<string, stack<set<string>>>> const &v, int m, string woord, string element)
{
  if (m < 1)
    return false;

  bool aanwezig = false;

  auto it = v[m - 1].find(woord);
  if (it != v[m - 1].end())
    if (!it->second.empty())
      aanwezig = (it->second.top().count(element) > 0);
  return aanwezig;
}

int main()
{
}
