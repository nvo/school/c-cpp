/*
Maak een vector aan van mappen. Elke map in die vector zal hetzelfde werk doen als de map
uit vorige oefening, maar dan enkel voor de woorden van bepaalde lengte: de i-de map uit de
vector houdt woorden van lengte i bij. (Het klopt dat de eerste en tweede map uit de vector dan
redelijk nutteloos zijn, maar laten we daar even abstractie van maken.)

Je maakt de vector van mappen maar net zolang als nodig. Dat wil zeggen dat je start bij lengte
10, en telkens je een woord tegenkomt dat toch langer is, resize je de vector van mappen.

Om zeker te zijn dat de vector van mappen goed opgevuld is, schrijf je hem uit.

Vraag de gebruiker nu om een woord, en schrijf alle woorden uit die met dezelfde letter starten en
even lang zijn. Wat uitgeschreven wordt moet niet noodzakelijk in alfabetische volgorde staan.

Verplaats het laatste deel (uitschrijven van alle woorden) naar een procedure zoek(woord, vector)
- er mag geen kopie genomen worden van de vector!!

Je kan het programma uittesten met het bestand bible_stop.txt, dat voor de gelegenheid
eindigt op het woord STOP. Gebruik inputredirection!
Omdat dit nu niet te combineren is met invoer vanop het klavier (bij input redirection is het
alles of niets!), mag je het zoekwoord hardcoderen: ga op zoek naar alle woorden in de bijbel
die even lang zijn als sinterklaas en ook met een s beginnen. (Je kan ook Sinterklaas met
hoofdletter opzoeken.) Merk op: de komma's en punten hangen nog aan de woorden vast, maar
dat is niet erg.
*/

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <unordered_set>
#include "../containers.h"

using namespace std;

const int INIT = 10;

void zoek(string const &word, vector<map<char, unordered_set<string>>> const &v)
{
  if (word.length() >= v.size())
  {
    cout << "geen woorden gevonden die zo lang zijn " << endl;
    return;
  }

  if (v[word.length()].count(word[0]) != 1)
  {
    cout << "geen woorden van die lengte met startletter " << word[0] << endl;
    return;
  }

  cout << v[word.length()].find(word[0])->second << endl;
}

int main()
{
  vector<map<char, unordered_set<string>>> woorden(INIT);
  string word, query;

  cout << "Geef woorden in (eindig met STOP):" << endl;
  cin >> word;
  while (word != "STOP")
  {
    if (woorden.size() <= word.length())
      woorden.resize(word.length() + 1);

    woorden[word.length()][word[0]].insert(word);
    cin >> word;
  }

  zoek("sinterklaas", woorden);

  // cout << "Geef een woord:" << endl;
  // cin >> query;

  // cout << woorden;
}