/*
Vooraf: herneem de methode print(pers) uit oefening 105. Deze methode is enkel bruikbaar
om informatie naar het scherm te schrijven.

In C++ kan de uitschrijfoperator << geïmplementeerd worden voor een willekeurige out-stream
(door overloading - dit komt later uitgebreid aan bod). Om gegevens van een persoon uit te
schrijven met de uitschrijfoperator denieer je onderstaande methode:

  ostream& operator<<(ostream& out, const persoon & p){
  out << p.naam << " (" << p.leeftijd << " jaar, "
  << (int)p.lengte <<"m" << (int)((p.lengte-1)*100) <<")";
  return out;
  }

Merk op: De uitschrijfoperator heeft ostream als eerste parameter, en geeft de gewijzigde
ostream terug.
De aanroep van de uitschrijfoperator is heel intuïtief:

  persoon p;
  initialiseer(p,"Jan",12,1.83);
  cout<<p;

In de volgende oefeningen gaan we containers vullen, raadplegen en veranderen. Het is handig
om methodes te hebben die de elementen van de verschillende containers (vector, stack, map,
set,...) uitschrijven (hetzij op scherm, hetzij naar een bestand).

Opdracht

1.  Schrijf de procedure print(v) die een vector van gehele getallen uitschrijft naar het scherm.

2.  Deze procedure werkt nu alleen voor vectoren met elementen van type int. Pas dit aan
    met templates.
    Controleer dat je met de procedure ook vectoren van type double, bool, string en char
    kan uitschrijven.
    Controleer de procedure ook voor een vector van persoon .

3.  Verander de procedure void print(...) naar een uitschrijfoperator << en test dit uit in
    het hoofdprogramma.

4.  Maak in het hoofdprogramma een vector v van vectoren aan, voeg aan v één vector toe die
    één element bevat. Schrijf de vector uit met cout<< .
*/

#include <string>
#include <iostream>
#include <vector>

using namespace std;

struct persoon
{
  string naam;
  int leeftijd;
  double lengte;
};

ostream &operator<<(ostream &out, const persoon &p)
{
  out << p.naam << " (" << p.leeftijd << " jaar, "
      << (int)p.lengte << "m" << (int)((p.lengte - 1) * 100) << ")";
  return out;
}

template <typename T>
void print(vector<T> const &v)
{
  for (auto &&i : v)
    cout << i << " ";
  cout << endl;
}

template <typename T>
ostream &operator<<(ostream &out, vector<T> const &v)
{
  for (auto &&i : v)
    out << i << " ";
  return out;
}

int main()
{
  vector<int> vi{1, 2, 3, 4, 5};
  vector<double> vd{.1, .2, .3, .4, .5};
  vector<bool> vb{false, true, false, true, false};
  vector<string> vs{"abc", "def", "ghi", "jkl", "mno"};
  vector<char> vc{'a', 'b', 'c', 'd', 'e'};
  vector<persoon> vp{
      {"Nick", 22, 1.68},
      {"Cedric", 20, 1.81},
      {"Mathijs", 19, 1.75}};
  vector<vector<int>> vv{{42}};

  // print(vi);
  // print(vd);
  // print(vb);
  // print(vs);
  // print(vc);
  // print(vp);

  cout << vi << endl
       << vd << endl
       << vb << endl
       << vs << endl
       << vc << endl
       << vp << endl
       << vv << endl;
}
