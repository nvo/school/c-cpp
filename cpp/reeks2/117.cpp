/*
Deze oefening vraagt geen code. Het enige wat je doet, is de declaratie neerschrijven van de
container(s) die je gaat gebruiken als je deze oefening zou moeten oplossen. (Voel je na afwerken
van de volledige reeks op containers nog de dringende nood aan meer oefening, dan heb je hier
meteen wat opdrachten.)

Elke opdracht mag onafhankelijk van de vorige beschouwd worden. Je krijgt telkens een bestand,
waarvoor je de opgesomde opdracht uitvoert.

(a) Geef het aantal verschillende woorden in het bestand.
unordered_set<string>

(b) Schrijf de woorden uit vanaf het i-de woord tot en met het j-de woord, waarbij i en j gekend
    zijn voor je aan de opdracht begint.
geen container, meteen uitschrijven

(c) Schrijf het bestand achterstevoren uit.
stack<string>
vector<string>

(d) Geef de frequentie van één woord dat gekend is voor je aan de opdracht begint.
geen container nodig, int als teller

(e) Geef de vindplaats(en) van één woord dat gekend is voor je aan de opdracht begint.
geen container nodig, meteen vindplaatsen uitschrijven.

(f) Geef de frequenties van alle woorden uit het bestand.
map<string, int>

(g) Geef de vindplaats(en) van alle woorden uit het bestand.
map<string, vector<int>>
*/