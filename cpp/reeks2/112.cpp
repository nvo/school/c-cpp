/*
Ga verder op de vorige oefening. Implementeer de uitschrijfoperator << voor volgende containers
met elementen van niet nader gespecieerd type. Je test telkens uit met een container waaraan
je één (maximaal twee) elementen toevoegt.

- een set (schrijf elementen uit tussen accolades, met een komma ertussen)
- een stack (schrijf elementen uit onder elkaar; begin een nieuwe regel voor het eerste element)
- een map, waarbij je dus zowel type van sleutel als type van bijhorende data ongespecieerd
  laat (schrijf sleutels onder elkaar uit; elke sleutel wordt gevolgd door een pijltje en zijn
  bijhorende data)
*/

#include <string>
#include <iostream>
#include <vector>
#include <set>
#include <stack>
#include <map>

using namespace std;

struct persoon
{
  string naam;
  int leeftijd;
  double lengte;
};

ostream &operator<<(ostream &out, const persoon &p)
{
  out << p.naam << " (" << p.leeftijd << " jaar, "
      << (int)p.lengte << "m" << (int)((p.lengte - 1) * 100) << ")";
  return out;
}

template <typename T>
ostream &operator<<(ostream &out, vector<T> const &v)
{
  auto it = v.begin();
  out << "[ " << *it++;
  for (; it != v.end(); it++)
    out << ", " << *it;
  out << " ]";
  return out;
}

template <typename T>
ostream &operator<<(ostream &out, set<T> const &s)
{
  auto it = s.begin();
  out << "{ " << *it++;
  for (; it != s.end(); it++)
    out << ", " << *it;
  out << " }";
  return out;
}

template <typename T>
ostream &operator<<(ostream &out, stack<T> &st)
{
  out << endl;
  while (!st.empty())
  {
    out << st.top() << endl;
    st.pop();
  }
  return out;
}

template <typename K, typename V>
ostream &operator<<(ostream &out, map<K, V> const &m)
{
  for (auto it = m.begin(); it != m.end(); it++)
    out << it->first << " -> " << it->second << endl;
  return out;
}

int main()
{
  vector<int> v{42, 73};
  set<int> s{69, 420};
  map<int, string> m{
      {1, "een"},
      {2, "twee"}};
  stack<int> st;

  // initialize stack
  for (auto &i : {1, 2})
    st.push(i);

  cout << "Vector: " << v << endl
       << "Set: " << s << endl
       << "Stack: " << st << endl
       << "Map: " << m << endl;
}
