/*
Gegeven volgend (gedeeltelijke) hoofdprogramma:

  int main() {
    unique_ptr<string> namen[] = {make_unique<string>("Rein"),
        make_unique<string>("Ada"), make_unique<string>("Eppo"),
        make_unique<string>("Pascal"), make_unique<string>("Ilse")};
    vector<unique_ptr<string>> namenvector;
    ... //voeg de unieke pointers van namen toe aan namenvector
    dupliceer(namenvector,3);
    ... //print de inhoud van namenvector
    //output moet zijn: Rein Rein Rein Ada Ada Ada Eppo Eppo Eppo Pascal Pascal Pascal Ilse Ilse Ilse
  }

Schrijf een procedure dupliceer(v,n) waarbij v een gegeven vector is met unieke pointers naar
elementen van een onbepaald type en n een geheel getal is, waarbij n > 1 . De procedure
dupliceert elk element n keer.

Vul daarna ook het hoofdprogramma verder aan.
*/

#include <string>
#include <iostream>
#include <memory>
#include <vector>
#include "../containers.h"

using namespace std;

template <typename T>
void dupliceer(vector<unique_ptr<T>> &v, int n)
{
  int orig = v.size();
  v.resize(v.size() * n);
  for (int i = orig - 1; i >= 0; i--)
  {
    int pos = i * n;
    v[pos] = move(v[i]);
    for (int j = pos + 1; j < pos + n; j++)
      v[j] = make_unique<T>(*v[pos]);
  }
}

int main()
{
  unique_ptr<string> namen[] = {make_unique<string>("Rein"),
                                make_unique<string>("Ada"), make_unique<string>("Eppo"),
                                make_unique<string>("Pascal"), make_unique<string>("Ilse")};
  vector<unique_ptr<string>> namenvector;

  for (int i = 0; i < 5; i++)
    namenvector.push_back(move(namen[i]));

  dupliceer(namenvector, 3);
  cout << namenvector;
  //output moet zijn: Rein Rein Rein Ada Ada Ada Eppo Eppo Eppo Pascal Pascal Pascal Ilse Ilse Ilse
}