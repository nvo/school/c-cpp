/*
Maak een map aan, die karakters afbeeldt op verzamelingen met woorden. Daarna lees je (vanop
klavier) een reeks woorden in die eindigt op STOP. De woorden worden volgens beginletter in de
map opgeslagen. Daarna vraag je de gebruiker een letter, en schrijf je uit hoeveel ingelezen
woorden er met deze letter beginnen. Je telt uiteraard alleen de verschillende woorden.
*/

#include <iostream>
#include <string>
#include <map>
#include <unordered_set>

using namespace std;

int main()
{
  map<char, unordered_set<string>> mapping;
  string word;

  cout << "Geef woorden in (eindig met STOP):" << endl;
  cin >> word;
  while (word != "STOP")
  {
    mapping[word[0]].insert(word);
    cin >> word;
  }
  fflush(stdin);

  char c;
  cout << "Geef letter in: ";
  c = getchar();

  cout << mapping[c].size() << " woorden beginnen met " << c << endl;
}
