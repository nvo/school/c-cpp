/*
In oefening 25 (reeks4) werd een functie als parameter doorgegeven aan een andere functie
of procedure. In C (en oudere C++) moet je die functie eerst expliciet een naam geven en
implementeren.

Vanaf C++11 kan het in één moeite: met lambda-functies maak je de functie 'on-the-fly' aan.

Pas onderstaand hoofdprogramma aan:

- schrijf de procedure schrijf.
- schrijf de procedure vul_array. .
  Let op, het type van de vierde parameter is nu geen functiepointer, want je werkt in
  C++11/14 in plaats van C!
- vervolledig de aanroep van de methode vul_array: vervang ...... door een -functie.
*/

#include <string>
#include <iostream>
#include <functional>

using namespace std;

void schrijf(string const &prefix, int const *t, int n)
{
  cout << prefix << t[0];
  for (int i = 1; i < n; i++)
    cout << " " << t[i];
  cout << endl;
}

void vul_array(int const *a, int const *b, int *c, int n, function<int(int a, int b)> f)
{
  for (int i = 0; i < n - 1; i++)
    c[i] = f(a[i], b[i]);
}

int main()
{
  const int GROOTTE = 10;

  int a[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  int b[] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90};
  int c[GROOTTE];

  vul_array(a, b, c, GROOTTE, [](int a, int b) { return a + b; });
  schrijf("SOM:\t\t", c, GROOTTE);

  vul_array(a, b, c, GROOTTE, [](int a, int b) { return a * b; });
  schrijf("PRODUCT:\t", c, GROOTTE);

  vul_array(a, b, c, GROOTTE, [](int a, int b) { return a - b; });
  schrijf("VERSCHIL:\t", c, GROOTTE);

  return 0;
}
