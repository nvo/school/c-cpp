/*
Onderstaande code is gegeven (haal de code uit het LATEX-bestand dat je op Ufora vindt; dan
zijn de kantlijnen netjes.)

De klasse Container<T> heeft twee private dataleden. Vul deze klasse verder aan zodat het
gegeven hoofdprogramma correct werkt.

De verwachte uitvoer van het hoofdprogramma zie je hieronder - maak deze letterlijk na:

*** set groot ***
{ a , b , c , d , e }
*** set klein ***
{ a , b , e , z }
*** set doorsnede groot en klein ***
{ a , b , e }
*/

#include <iostream>
#include <set>
#include <map>
#include "../containers.h"
using namespace std;

template <typename T>
class Container
{
private:
  set<T> elementen;
  string naam;

public:
  Container(){};
  Container(T const *tab, int n, string naam) : elementen{tab, tab + n}, naam{naam} {};
  Container<T> &operator+=(T const &e);
  Container<T> operator*(Container<T> const &other) const;

  template <typename U>
  friend ostream &operator<<(ostream &os, Container<U> const &c);
};

template <typename T>
Container<T> &Container<T>::operator+=(T const &e)
{
  this->elementen.insert(e);
  return *this;
}

template <typename T>
Container<T> Container<T>::operator*(Container<T> const &other) const
{
  Container<T> n;
  n.naam = "set doorsnede " + this->naam + " en " + other.naam;

  for (T const &e : this->elementen)
    if (other.elementen.count(e) > 0)
      n.elementen.insert(e);

  return n;
}

template <typename T>
ostream &operator<<(ostream &os, Container<T> const &c)
{
  os << "*** " << c.naam << " ***" << endl
     << c.elementen << endl;
  return os;
}

int main()
{
  char v[] = {'a', 'b', 'c', 'd', 'e', 'e'};
  Container<char> verz1(v, 6, "groot");
  Container<char> verz2(v, 2, "klein");

  verz2 += 'e';
  verz2 += 'z';

  Container<char> doorsnede = verz1 * verz2; //Maak de doorsnede

  cout << verz1;
  cout << verz2;
  cout << doorsnede;

  return 0;
}