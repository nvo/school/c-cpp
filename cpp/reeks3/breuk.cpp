#include <string>
#include <cmath>
#include <iostream>

int mijn_ggd(int a, int b);

class Breuk
{
private:
  int teller, noemer;

public:
  Breuk(int t = 1, int n = 1);
  Breuk operator-() const;
  Breuk operator+(Breuk const &b) const;
  Breuk operator-(Breuk const &b) const;
  Breuk &operator+=(Breuk const &b);
  Breuk &operator-=(Breuk const &b);
  Breuk &operator++();
  Breuk operator++(int);
  bool operator==(Breuk const &b) const;
  bool operator!=(Breuk const &b) const;
  bool operator<(Breuk const &b) const;
  void normaliseer();
  friend bool is_stambreuk(Breuk const &b);
  friend Breuk operator+(int i, Breuk const &b);
  friend std::ostream &operator<<(std::ostream &os, Breuk const &b);
  friend std::istream &operator>>(std::istream &is, Breuk &b);
};

Breuk::Breuk(int t, int n) : teller{t}, noemer{n}
{
  this->normaliseer();
}

Breuk Breuk::operator-() const
{
  Breuk c{-this->teller, this->noemer};
  return c;
}

Breuk Breuk::operator+(Breuk const &b) const
{
  Breuk c{
      this->teller * b.noemer + b.teller * this->noemer,
      this->noemer * b.noemer};
  return c;
}

Breuk Breuk::operator-(Breuk const &b) const
{
  Breuk c{
      this->teller * b.noemer - b.teller * this->noemer,
      this->noemer * b.noemer};
  return c;
}

Breuk &Breuk::operator+=(Breuk const &b)
{
  this->teller = this->teller * b.noemer + b.teller * this->noemer;
  this->noemer *= b.noemer;
  this->normaliseer();
  return *this;
}

Breuk &Breuk::operator-=(Breuk const &b)
{
  this->teller = this->teller * b.noemer - b.teller * this->noemer;
  this->noemer *= b.noemer;
  this->normaliseer();
  return *this;
}

Breuk &Breuk::operator++()
{
  this->teller += this->noemer;
  return *this;
}

Breuk Breuk::operator++(int)
{
  this->teller += this->noemer;
  return *this;
}

bool Breuk::operator==(Breuk const &b) const
{
  return (this->teller * b.noemer) == (this->noemer * b.teller);
}

bool Breuk::operator!=(Breuk const &b) const
{
  return (this->teller * b.noemer) != (this->noemer * b.teller);
}

bool Breuk::operator<(Breuk const &b) const
{
  return this->teller * b.noemer < this->noemer * b.teller;
}

void Breuk::normaliseer()
{
  int ggd = mijn_ggd(this->teller, this->noemer);
  this->teller /= ggd;
  this->noemer /= ggd;

  if (this->noemer < 0)
  {
    this->noemer *= -1;
    this->teller *= -1;
  }
}

Breuk operator+(int i, Breuk const &b)
{
  Breuk c{
      b.teller + b.noemer * i,
      b.noemer};
  return c;
}

std::ostream &operator<<(std::ostream &os, Breuk const &b)
{
  os << b.teller;
  if (b.noemer != 1)
    os << "/" << b.noemer;
  return os;
}

std::istream &operator>>(std::istream &is, Breuk &b)
{
  std::string line;
  std::getline(is, line);

  int p = line.find('/');
  if (p != std::string::npos)
    b = Breuk(atoi(line.substr(0, p).c_str()), atoi(line.substr(p + 1).c_str()));
  else
    b = Breuk(atoi(line.c_str()));

  return is;
}

bool is_stambreuk(Breuk const &b)
{
  return b.teller == 1;
}

// zet deze functie in het bestand 'breuk.cpp'
int mijn_ggd(int a, int b)
{
  if (a < 0 || b < 0)
  {
    return mijn_ggd(abs(a), abs(b));
  }
  if (b == 0)
  {
    return a;
  }
  return mijn_ggd(b, a % b);
}
